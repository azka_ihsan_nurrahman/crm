@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Detail Data Pelanggan</h3>
				<div class="form-group">
					<div class="col-lg-3 control-label">Nama Customer</div>
					<div class="col-lg-9"> {{ $customer->customer_name }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Tanggal Lahir</div>
					<div class="col-lg-9"> {{ date('d-m-Y', strtotime($customer->birth_date)) }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Umur</div>
					<div class="col-lg-9"> {{ $customer->age }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Gender</div>
					<div class="col-lg-9"> {{ $customer->gender }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Pekerjaan</div>
					<div class="col-lg-9"> {{ $customer->profession }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Status Pernikahan</div>
					<div class="col-lg-9"> {{ $customer->marital_status }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Email</div>
					<div class="col-lg-9"> {{ $customer->email }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Alamat</div>
					<div class="col-lg-9"> {{ $customer->address }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Kota</div>
					<div class="col-lg-9"> {{ $customer->city }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Nomor Telepon</div>
					<div class="col-lg-9"> {{ $customer->phone_number }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">QR Code</div>
					<div class="col-lg-9"> <img src="{{ asset($customer->QR_Code) }}" alt="No QR Code"></div>
				</div>
				<br>
				<a class="btn btn-primary" href="{{ route('customer.index') }}" role="button">Kembali</a>
		</div>
	</div>
</div>
@endsection

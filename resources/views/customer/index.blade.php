@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col">
			<h3>Daftar Pelanggan</h3>
			@if(Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama Customer</th>
						<th>Usia</th>
						<th>Pekerjaan</th>
						<th>Email</th>
						<th>Nomor Telepon</th>			
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($customers as $customer)
					<tr>
						<td>{{ $customer->id }}</td>
						<td>{{ $customer->customer_name }}</td>
						<td>{{ $customer->age }}</td>
						<td>{{ $customer->profession }}</td>
						<td>{{ $customer->email }}</td>
						<td>{{ $customer->phone_number }}</td>
						<td>
							<a href="{{ route('customer.show', $customer->id) }}" class="btn btn-warning">View</a>
						</td>
					</tr>
					@empty
					    <td colspan="7">Belum ada data</td>
					@endforelse
				</tbody>
			</table>
			{!! $customers->links() !!}
		</div>
	</div>
</div>
@endsection
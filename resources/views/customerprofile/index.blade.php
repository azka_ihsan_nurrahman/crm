@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h3>Dashboard Customer Profile</h3>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Segmentasi Customer Berdasarkan Gender</div>
                <div class="panel-body">
                    <a href="{{ route('customerreport.showgendersegment', 'Pria') }}">Pria </a> : {{ $malecustomers }}
                    <br>
                    <a href="{{ route('customerreport.showgendersegment', 'Wanita') }}">Wanita </a> : {{ $femalecustomers }}
                    <br>
                </div>
            </div>
        </div>
       <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Segmentasi Customer Berdasarkan Usia</div>
                <div class="panel-body">
                    <a href="{{ route('customerreport.showagesegment', 'anak') }}">Anak-Anak (di bawah 14 tahun) </a> : {{ $childcustomers }}
                    <br>
                    <a href="{{ route('customerreport.showagesegment', 'remaja') }}">Remaja (14-21 tahun) </a> : {{ $teencustomers }}
                    <br>
                    <a href="{{ route('customerreport.showagesegment', 'produktif') }}">Dewasa Usia Produktif (22-40 tahun) </a> : {{ $productivecustomers }}
                    <br>
                    <a href="{{ route('customerreport.showagesegment', 'dewasa') }}">Dewasa Usia Lanjut (41-60 tahun) </a> : {{ $adultcustomers }}
                    <br>
                    <a href="{{ route('customerreport.showagesegment', 'lansia') }}">Lansia (di atas 60 tahun) </a> : {{ $eldercustomers }}
                    <br>
                </div>
            </div>
        </div>
       <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Segmentasi Customer Berdasarkan Status Pernikahan</div>
                <div class="panel-body">
                    <a href="{{ route('customerreport.showmaritalsegment', 'belum') }}">Belum Menikah </a> : {{ $singlecustomers }}
                    <br>
                    <a href="{{ route('customerreport.showmaritalsegment', 'sudah') }}">Sudah Menikah </a> : {{ $marriedcustomers }}
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Segmentasi Customer Berdasarkan Profesi</div>
                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Profesi</th>
                                <th>Jumlah Customer</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($customerprofession as $row)
                            <tr>
                                <td>{{ $row['profession'] }}</td>
                                <td>{{ $row['count'] }}</td>
                            </tr>
                            @empty
                                <td colspan="2">Belum ada data</td>
                            @endforelse
                        </tbody>
                    </table>
                    <a href="{{ route('customerreport.professionsegmentreport') }}" class="btn btn-warning">Cetak Laporan</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Segmentasi Customer Berdasarkan Wilayah</div>
                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Wilayah</th>
                                <th>Jumlah Customer</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($customercity as $row)
                            <tr>
                                <td>{{ $row['city'] }}</td>
                                <td>{{ $row['count'] }}</td>
                            </tr>
                            @empty
                                <td colspan="2">Belum ada data</td>
                            @endforelse
                        </tbody>
                    </table>
                    <a href="{{ route('customerreport.citysegmentreport') }}" class="btn btn-warning">Cetak Laporan</a>
                </div>
            </div>
        </div>
    </div>
    <a href="{{ route('home') }}" class="btn btn-primary">Kembali</a>
</div>
@endsection
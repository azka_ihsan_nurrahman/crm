@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col">
			<h3>Daftar Pelanggan Berdasarkan Segmen Status Pernikahan</h3>
			<h4>{{ $marital }}</h4>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama Customer</th>
						<th>Usia</th>
						<th>Pekerjaan</th>
						<th>Email</th>
						<th>Nomor Telepon</th>			
					</tr>
				</thead>
				<tbody>
					@forelse ($customers as $customer)
					<tr>
						<td>{{ $customer->id }}</td>
						<td>{{ $customer->customer_name }}</td>
						<td>{{ $customer->age }}</td>
						<td>{{ $customer->profession }}</td>
						<td>{{ $customer->email }}</td>
						<td>{{ $customer->phone_number }}</td>
					</tr>
					@empty
					    <td colspan="6">Belum ada data</td>
					@endforelse
				</tbody>
			</table>
			{!! $customers->links() !!}
			<a href="{{ route('customerreport.reportmaritalsegment', $marital) }}" class="btn btn-warning">Cetak Laporan</a>
			<a href="{{ route('customerreport.index') }}" class="btn btn-primary">Kembali</a>
		</div>
	</div>
</div>
@endsection
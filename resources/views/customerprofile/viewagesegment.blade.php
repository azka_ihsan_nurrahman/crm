@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col">
			<h3>Daftar Pelanggan Berdasarkan Segmen Usia</h3>
			@if ($age == "anak")
			<h4>Anak-Anak</h4>
			@elseif ($age == "remaja")
			<h4>Remaja</h4>
			@elseif ($age == "produktif")
			<h4>Dewasa Produktif</h4>
			@elseif ($age == "dewasa")
			<h4>Dewasa Lanjut</h4>
			@elseif ($age == "lansia")
			<h4>Lansia</h4>
			@endif
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama Customer</th>
						<th>Pekerjaan</th>
						<th>Status Pernikahan</th>
						<th>Email</th>
						<th>Nomor Telepon</th>			
					</tr>
				</thead>
				<tbody>
					@forelse ($customers as $customer)
					<tr>
						<td>{{ $customer->id }}</td>
						<td>{{ $customer->customer_name }}</td>
						<td>{{ $customer->profession }}</td>
						<td>{{ $customer->marital_status }}</td>
						<td>{{ $customer->email }}</td>
						<td>{{ $customer->phone_number }}</td>
					</tr>
					@empty
					    <td colspan="6">Belum ada data</td>
					@endforelse
				</tbody>
			</table>
			{!! $customers->links() !!}
			<a href="{{ route('customerreport.reportagesegment', $age) }}" class="btn btn-warning">Cetak Laporan</a>
			<a href="{{ route('customerreport.index') }}" class="btn btn-primary">Kembali</a>
		</div>
	</div>
</div>
@endsection
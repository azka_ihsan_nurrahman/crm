<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>Laporan Profil Customer</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col">
			<h3 align="center">Laporan Profil Customer</h3>
			<h4 align="center">CRM-System</h4>
			<h4 align="center">Segmentasi Status Pernikahan Customer</h4>
			<h4 align="center">{{ $marital }}</h4>
			<table border="3">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama Customer</th>
						<th>Usia</th>
						<th>Pekerjaan</th>
						<th>Email</th>
						<th>Nomor Telepon</th>			
					</tr>
				</thead>
				<tbody>
					@forelse ($customers as $customer)
					<tr>
						<td>{{ $customer->id }}</td>
						<td>{{ $customer->customer_name }}</td>
						<td>{{ $customer->age }}</td>
						<td>{{ $customer->profession }}</td>
						<td>{{ $customer->email }}</td>
						<td>{{ $customer->phone_number }}</td>
					</tr>
					@empty
					    <td colspan="6">Belum ada data</td>
					@endforelse
				</tbody>
			</table>
			<h4 align="right">
				@php
				echo date('d-m-Y H:i:s');
				@endphp
			</h4>
		</div>
	</div>
</div>
</body>
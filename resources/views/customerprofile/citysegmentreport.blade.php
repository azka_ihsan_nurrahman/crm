<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>Laporan Profil Customer</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col">
			<h3 align="center">Laporan Profil Customer</h3>
			<h4 align="center">CRM-System</h4>
			<h4 align="center">Segmentasi Wilayah Customer</h4>
			<table border="3">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Wilayah</th>
						<th>Jumlah Customer</th>
					</tr>
				</thead>
				<tbody>
					@php
					$total = 0;
					$nomor = 1;
					@endphp
					@forelse ($customercity as $row)
					@php
					$total = $total + $row['count'];
					@endphp
					<tr>
						<td>{{ $nomor }}</td>
						<td>{{ $row['city'] }}</td>
						<td>{{ $row['count'] }}</td>
					</tr>
					@php
					$nomor = $nomor + 1;
					@endphp
					@empty
					    <td colspan="3">Belum ada data</td>
					@endforelse
					<tr>
						<td colspan="2"><b>Total Customer</b></td>
						<td><b>{{ $total }}</b></td>
					</tr>
				</tbody>
			</table>
			<h4 align="right">
				@php
				echo date('d-m-Y H:i:s');
				@endphp
			</h4>
		</div>
	</div>
</div>
</body>
@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Edit Data Shopkeeper</h3>
			<form action="{{ route('shopkeeper.update', $shopkeeper->id) }}" method="post">
				
				{{ csrf_field() }}
				{{ method_field('put') }}

				<div class="form-group">
					<label>Nama Shopkeeper</label>
					<input type="text" name="shopkeeper_name" class="form-control" value="{{ $shopkeeper->shopkeeper_name}}">
				</div>
				<div class="form-group">
					<label>Nama Owner</label>
					<input type="text" name="owner_name" class="form-control" value="{{ $shopkeeper->owner_name}}">
				</div>
				<div class="form-group">
					<label>Nomor KTP Owner</label>
					<input type="text" name="owner_ktp" class="form-control" value="{{ $shopkeeper->owner_ktp}}">
				</div>
				<div class="form-group">
					<label>Alamat</label>
					<input type="textarea" name="address" class="form-control" value="{{ $shopkeeper->address}}">
				</div>
				<div class="form-group">
					<label>Kota</label>
					<input type="text" name="city" class="form-control" value="{{ $shopkeeper->city}}">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
				<a class="btn btn-primary" href="{{ route('shopkeeper.index') }}" role="button">Kembali</a>
			</form>
		</div>
	</div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Daftar Shopkeeper</h3>
			@if(Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<a href="{{ route('shopkeeper.create') }}" class="btn btn-primary">Tambah Shopkeeper</a>
			<br></br>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama Shopkeeper</th>
						<th>Nama Pemilik</th>
						<th>Nomor KTP</th>			
						<th>Status</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($shopkeepers as $shopkeeper)
					<tr>
						<td>{{ $shopkeeper->id }}</td>
						<td>{{ $shopkeeper->shopkeeper_name }}</td>
						<td>{{ $shopkeeper->owner_name }}</td>
						<td>{{ $shopkeeper->owner_ktp }}</td>
						<td>{{ $shopkeeper->status }}</td>
						<td>
							<a href="{{ route('shopkeeper.show', $shopkeeper->id) }}" class="btn btn-warning">View</a>
							<a href="{{ route('shopkeeper.edit', $shopkeeper->id) }}" class="btn btn-primary">Edit</a>
							<a href="{{ route('shopkeeper.delete', $shopkeeper->id) }}" class="btn btn-danger">Delete</a>
						</td>
					</tr>
					@empty
					    <td colspan="6">Belum ada data</td>
					@endforelse
				</tbody>
			</table>
			{!! $shopkeepers->links() !!}
		</div>
	</div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Tambah Shopkeeper Baru</h3>
			@if ($errors->count() > 0)
			<div class="alert alert-danger">
				@foreach ($errors->all() as $message)
				    <li>{{ $message }}</li>
				@endforeach
			</div>
			@endif
			<form action="{{ route('shopkeeper.store') }}" method="post">
				
				{{ csrf_field() }}

				<div class="form-group">
					<label>Nama Shopkeeper</label>
					<input type="text" name="shopkeeper_name" class="form-control" value="{{ old('shopkeeper_name') }}">
				</div>
				<div class="form-group">
					<label>Nama Owner</label>
					<input type="text" name="owner_name" class="form-control" value="{{ old('owner_name') }}">
				</div>
				<div class="form-group">
					<label>Nomor KTP Owner</label>
					<input type="text" name="owner_ktp" class="form-control" value="{{ old('owner_ktp') }}">
				</div>
				<div class="form-group">
					<label>Alamat</label>
					<input type="textarea" name="address" class="form-control" value="{{ old('address') }}">
				</div>
				<div class="form-group">
					<label>Kota</label>
					<input type="text" name="city" class="form-control" value="{{ old('city') }}">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
				<a class="btn btn-primary" href="{{ route('shopkeeper.index') }}" role="button">Kembali</a>
			</form>
		</div>
	</div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Detail Data Shopkeeper</h3>
				<div class="form-group">
					<div class="col-lg-3 control-label">Nama Shopkeeper</div>
					<div class="col-lg-9"> {{ $shopkeeper->shopkeeper_name}} </div>

				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Nama Owner</div>
					<div class="col-lg-9"> {{ $shopkeeper->owner_name}} </div>

				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Nomor KTP Owner</div>
					<div class="col-lg-9"> {{ $shopkeeper->owner_ktp}} </div>

				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Alamat</div>
					<div class="col-lg-9"> {{ $shopkeeper->address}} </div>

				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Kota</div>
					<div class="col-lg-9"> {{ $shopkeeper->city}} </div>

				</div>
				<br></br>
			<form action="{{ route('shopkeeperstatus.update', $shopkeeper->id) }}" method="post">
				
				{{ csrf_field() }}
				{{ method_field('put') }}

				<div class="form-group">
					<label>Status</label>
					<label class="radio-inline"><input type="radio" name="status" value="Aktif" {{ ($shopkeeper->status=="Aktif")? "checked" : "" }}>Aktif</label>
					<label class="radio-inline"><input type="radio" name="status" value="Nonaktif" {{ ($shopkeeper->status=="Nonaktif")? "checked" : "" }}>Nonaktif</label>
				</div>
				<button type="submit" class="btn btn-primary">Ubah Status Shopkeeper</button>
				<a class="btn btn-primary" href="{{ route('shopkeeper.index') }}" role="button">Kembali</a>
			</form>
		</div>
	</div>
</div>
@endsection

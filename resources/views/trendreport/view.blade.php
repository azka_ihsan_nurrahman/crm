@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Laporan Trend</h3>
			@if ($group == "customer")
			    <h4>Customer</h4>
			@elseif ($group == "loyaltyprogram")
			    <h4>Loyalty Program</h4>
			@elseif ($group == "item")
			    <h4>Item</h4>
			@elseif ($group == "shopkeeper")
				<h4>Shopkeeper</h4>
			@endif
			@if(Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						@if ($group == "customer")
							<th>Nama Customer</th>
							<th>Frekuensi Belanja</th>
						@elseif ($group == "loyaltyprogram")
							<th>Nama Program</th>
							<th>Frekuensi Pemakaian</th>
						@elseif ($group == "item")
							<th>Nama Barang</th>
							<th>Jumlah Terjual</th>
						@elseif ($group == "shopkeeper")
							<th>Nama Shopkeeper</th>
							<th>Frekuensi Transaksi</th>
						@endif
					</tr>
				</thead>
				<tbody>
					@php
					$nomor = 1;
					@endphp
					@forelse ($data as $datum)
					<tr>
						<td>{{ $nomor }}</td>
						<td>{{ $datum['name'] }}</td>
						<td>{{ $datum['count'] }}</td>
					</tr>
					@php
					$nomor = $nomor + 1;
					@endphp
					@empty
					    <td colspan="3">Belum ada data</td>
					@endforelse
					<tr>
						<td colspan="3">
							<a href="{{ route('trendreport.printtrendreport', $group) }}" class="btn btn-warning">Cetak Laporan</a>
							<a href="{{ route('trendreport.index') }}" class="btn btn-primary">Kembali</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col">
			
		</div>
	</div>
</div>
@endsection
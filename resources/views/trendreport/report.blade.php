<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>Laporan Omzet</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col">
			<h3 align="center">Laporan Trend</h3>
			<h4 align="center">CRM-System</h4>
			@if ($group == "customer")
			    <h4 align="center">Customer</h4>
			@elseif ($group == "loyaltyprogram")
			    <h4 align="center">Loyalty Program</h4>
			@elseif ($group == "item")
			    <h4 align="center">Item</h4>
			@elseif ($group == "shopkeeper")
				<h4 align="center">Shopkeeper</h4>
			@endif
			<table table border="3">
				<thead>
					<tr>
						<th>Nomor</th>
						@if ($group == "customer")
							<th>Nama Customer</th>
							<th>Frekuensi Belanja</th>
						@elseif ($group == "loyaltyprogram")
							<th>Nama Program</th>
							<th>Frekuensi Pemakaian</th>
						@elseif ($group == "item")
							<th>Nama Barang</th>
							<th>Jumlah Terjual</th>
						@elseif ($group == "shopkeeper")
							<th>Nama Shopkeeper</th>
							<th>Frekuensi Transaksi</th>
						@endif
					</tr>
				</thead>
				<tbody>
					@php
					$nomor = 1;
					@endphp
					@forelse ($data as $datum)
					<tr>
						<td>{{ $nomor }}</td>
						<td>{{ $datum['name'] }}</td>
						<td>{{ $datum['count'] }}</td>
					</tr>
					@php
					$nomor = $nomor + 1;
					@endphp
					@empty
					    <td colspan="3">Belum ada data</td>
					@endforelse
				</tbody>
			</table>
			<h4 align="right">
				@php
				echo date('d-m-Y H:i:s');
				@endphp
			</h4>
		</div>
	</div>
</div>
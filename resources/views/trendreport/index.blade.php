@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h3>Dashboard Trend</h3>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Customer dengan Belanja Terbanyak</div>
                <div class="panel-body">
                    Nama Customer : {{ $customerrankmax["name"] }}
                    <br>
                    Frekuensi Belanja : {{ $customerrankmax["count"] }}
                    <br>
                    <br>
                    <a href="{{ route('trendreport.show', 'customer') }}" class="btn btn-primary">Lihat Detail</a>
                </div>
            </div>
        </div>
       <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Shopkeeper Paling Sering Dikunjungi</div>
                <div class="panel-body">
                    Nama Shopkeeper : {{ $shopkeeperrankmax["name"] }}
                    <br>
                    Frekuensi Transaksi : {{ $shopkeeperrankmax["count"] }}
                    <br>
                    <br>
                    <a href="{{ route('trendreport.show', 'shopkeeper') }}" class="btn btn-primary">Lihat Detail</a>
                </div>
            </div>
        </div>
       <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Barang Terlaku</div>
                <div class="panel-body">
                    Nama Barang : {{ $itemrankmax["name"] }}
                    <br>
                    Jumlah Terjual : {{ $itemrankmax["count"] }}
                    <br>
                    <br>
                    <a href="{{ route('trendreport.show', 'item') }}" class="btn btn-primary">Lihat Detail</a>
                </div>
            </div>
        </div>
       <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Loyalty Program Terlaku</div>
                <div class="panel-body">
                    Nama Program : {{ $loyaltyprogramrankmax["name"] }}
                    <br>
                    Frekuensi Pemakaian : {{ $loyaltyprogramrankmax["count"] }}
                    <br>
                    <br>
                    <a href="{{ route('trendreport.show', 'loyaltyprogram') }}" class="btn btn-primary">Lihat Detail</a>
                </div>
            </div>
        </div>
    </div>
    <a href="{{ route('home') }}" class="btn btn-primary">Kembali</a>
</div>
@endsection
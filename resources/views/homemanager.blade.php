@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Dashboard Manager</h3>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Customer dengan Belanja Terbanyak</div>
                <div class="panel-body">
                    Nama Customer : {{ $datatrend["customerrankmax"]["name"] }}
                    <br>
                    Frekuensi Belanja : {{ $datatrend["customerrankmax"]["count"] }}
                </div>
            </div>
        </div>
       <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Shopkeeper Paling Sering Dikunjungi</div>
                <div class="panel-body">
                    Nama Shopkeeper : {{ $datatrend["shopkeeperrankmax"]["name"] }}
                    <br>
                    Frekuensi Transaksi : {{ $datatrend["shopkeeperrankmax"]["count"] }}
                </div>
            </div>
        </div>
       <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Barang Terlaku</div>
                <div class="panel-body">
                    Nama Barang : {{ $datatrend["itemrankmax"]["name"] }}
                    <br>
                    Jumlah Terjual : {{ $datatrend["itemrankmax"]["count"] }}
                </div>
            </div>
        </div>
       <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Loyalty Program Terlaku</div>
                <div class="panel-body">
                    Nama Program : {{ $datatrend["loyaltyprogramrankmax"]["name"] }}
                    <br>
                    Frekuensi Pemakaian : {{ $datatrend["loyaltyprogramrankmax"]["count"] }}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Segmentasi Customer Berdasarkan Gender</div>
                <div class="panel-body">
                    Pria : {{ $customerprofiledata['malecustomers'] }}
                    <br>
                    Wanita : {{ $customerprofiledata['femalecustomers'] }}
                    <br>
                </div>
            </div>
        </div>
       <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Segmentasi Customer Berdasarkan Usia</div>
                <div class="panel-body">
                    Anak-Anak (di bawah 14 tahun) : {{ $customerprofiledata['childcustomers'] }}
                    <br>
                    Remaja (14-21 tahun) : {{ $customerprofiledata['teencustomers'] }}
                    <br>
                    Dewasa Usia Produktif (22-40 tahun) : {{ $customerprofiledata['productivecustomers'] }}
                    <br>
                    Dewasa Usia Lanjut (41-60 tahun) : {{ $customerprofiledata['adultcustomers'] }}
                    <br>
                    Lansia (di atas 60 tahun) : {{ $customerprofiledata['eldercustomers'] }}
                    <br>
                </div>
            </div>
        </div>
       <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Segmentasi Customer Berdasarkan Status Pernikahan</div>
                <div class="panel-body">
                    Belum Menikah : {{ $customerprofiledata['singlecustomers'] }}
                    <br>
                    Sudah Menikah : {{ $customerprofiledata['marriedcustomers'] }}
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard Keuangan & Pembayaran</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Metode Pembayaran</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Cash</td>
                                <td>Rp {{ number_format($datafinance['totalcash'], 2, ',', '.') }}</td>
                            </tr>
                            <tr>
                                <td>Debit</td>
                                <td>Rp {{ number_format($datafinance['totaldebit'], 2, ',', '.') }}</td>
                            </tr>
                            <tr>
                                <td>Credit</td>
                                <td>Rp {{ number_format($datafinance['totalcredit'], 2, ',', '.') }}</td>
                            </tr>
                            <tr>
                                <td><b>Total Omzet</b></td>
                                <td><b>Rp {{ number_format($datafinance['totalomzet'], 2, ',', '.') }}</b></td>
                            </tr>
                            <tr>
                                <td><b>Total Transaksi</b></td>
                                <td><b>{{ $datafinance['totaltransaction'] }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
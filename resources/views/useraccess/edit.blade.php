@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Ubah Akses Akun</h3>
				<div class="form-group">
					<div class="col-lg-3 control-label">Nama</div>
					<div class="col-lg-9"> {{ $user->name }} </div>

				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Email</div>
					<div class="col-lg-9"> {{ $user->email }} </div>

				</div>
				<br></br>
			<form action="{{ route('useraccess.update', $user->id) }}" method="post">
				
				{{ csrf_field() }}
				{{ method_field('put') }}

				<div class="form-group">
					<label>Level Akun </label>
					<label class="radio-inline"><input type="radio" name="account_level" value="2" {{ ($user->account_level==2)? "checked" : "" }}>Staff</label>
					<label class="radio-inline"><input type="radio" name="account_level" value="3" {{ ($user->account_level==3)? "checked" : "" }}>Manager</label>
				</div>
				<button type="submit" class="btn btn-primary">Ubah Level Akses</button>
				<a class="btn btn-primary" href="{{ route('useraccess.index') }}" role="button">Kembali</a>
			</form>
		</div>
	</div>
</div>
@endsection

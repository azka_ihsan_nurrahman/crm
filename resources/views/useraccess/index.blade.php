@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col">
			<h3>Daftar Akun Pengguna</h3>
			@if(Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama</th>
						<th>Email</th>
						<th>Level Akun</th>		
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($users as $user)
					<tr>
						<td>{{ $user->id }}</td>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>@if ($user->account_level == 1) Customer
							@endif
							@if ($user->account_level == 2) Staff
							@endif
							@if ($user->account_level == 3) Manager
							@endif
							@if ($user->account_level == 4) Admin
							@endif
						</td>
						<td>
							@if ($user->account_level == 2) 
							<a href="{{ route('useraccess.edit', $user->id) }}" class="btn btn-warning">Ubah Akses Akun</a>
							@endif
							@if ($user->account_level == 3)
							<a href="{{ route('useraccess.edit', $user->id) }}" class="btn btn-warning">Ubah Akses Akun</a>
							@endif
						</td>
					</tr>
					@empty
					    <td colspan="5">Belum ada data</td>
					@endforelse
				</tbody>
			</table>
			{!! $users->links() !!}
		</div>
	</div>
</div>
@endsection
<h1>Forget Password</h1>

<p>Here is your reset password link : </p>
<a href="{{ $link }}" target="_blank"><strong>Reset Password</strong></a>
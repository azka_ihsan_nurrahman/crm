@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Daftar Log Akses</h3>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>User ID</th>
						<th>User Name</th>
						<th>IP Address</th>
						<th>User Agent</th>			
						<th>Last Activity</th>
					</tr>
				</thead>
				<tbody>
					@php
					$i=1
					@endphp
					@forelse ($data as $datum)
					<tr>
						<td>{{ $i }}</td>
						<td>{{ $datum->user_id }}</td>
						<td>{{ $datum->user->name }}</td>
						<td>{{ $datum->ip_address }}</td>
						<td>{{ $datum->user_agent }}</td>
						<td>{{ date("Y-m-d H:i:s",$datum->last_activity) }}</td>
					</tr>
					@php
					$i++
					@endphp
					@empty
					    <td colspan="6">Belum ada data</td>
					@endforelse
				</tbody>
			</table>
			{!! $data->links() !!}
		</div>
	</div>
</div>
@endsection
@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col">
			<h3>Daftar Barang</h3>
			@if(Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<a href="{{ route('item.create') }}" class="btn btn-primary">Tambah Barang</a>
			<br></br>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama Barang</th>
						<th>Harga</th>
						<th>Stok</th>			
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($items as $item)
					<tr>
						<td>{{ $item->id }}</td>
						<td>{{ $item->item_name }}</td>
						<td>Rp {{ number_format($item->price, 2, ',', '.') }}</td>
						<td>{{ $item->stock }}</td>
						<td>
							<a href="{{ route('item.show', $item->id) }}" class="btn btn-warning">View</a>
							<a href="{{ route('upload.picture', $item->id) }}" class="btn btn-info">Upload</a>
							<a href="{{ route('item.edit', $item->id) }}" class="btn btn-primary">Edit</a>
							<a href="{{ route('item.delete', $item->id) }}" class="btn btn-danger">Delete</a>
						</td>
					</tr>
					@empty
					    <td colspan="5">Belum ada data</td>
					@endforelse
				</tbody>
			</table>
			{!! $items->links() !!}
		</div>
	</div>
</div>
@endsection
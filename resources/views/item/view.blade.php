@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Detail Data Barang</h3>
				<div class="form-group">
					<div class="col-lg-3 control-label">Nama Barang</div>
					<div class="col-lg-9"> {{ $item->item_name}} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Harga</div>
					<div class="col-lg-9"> Rp {{ number_format($item->price, 2, ',', '.') }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Stok</div>
					<div class="col-lg-9"> {{ $item->stock}} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Deskripsi</div>
					<div class="col-lg-9"> {{ $item->description}} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Gambar</div>
					<div class="col-lg-9"> <img src="{{ asset($item->picture_url) }}" alt="No Picture"></div>
				</div>
				<br>
				<a class="btn btn-primary" href="{{ route('item.index') }}" role="button">Kembali</a>
		</div>
	</div>
</div>
@endsection

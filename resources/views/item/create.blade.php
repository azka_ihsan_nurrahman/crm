@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Tambah Barang</h3>
			@if ($errors->count() > 0)
			<div class="alert alert-danger">
				@foreach ($errors->all() as $message)
				    <li>{{ $message }}</li>
				@endforeach
			</div>
			@endif
			<form action="{{ route('item.store') }}" method="post">
				
				{{ csrf_field() }}

				<div class="form-group">
					<label>Nama Barang</label>
					<input type="text" name="item_name" class="form-control" value="{{ old('item_name') }}">
				</div>
				<div class="form-group">
					<label>Harga</label>
					<input type="number" name="price" min="0" class="form-control" value="{{ old('price') }}">
				</div>
				<div class="form-group">
					<label>Stok</label>
					<input type="number" name="stock" min="1" class="form-control" value="{{ old('stock') }}">
				</div>
				<div class="form-group">
					<label>Deskripsi Barang</label>
					<input type="textarea" name="description" class="form-control" value="{{ old('description') }}">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
				<a class="btn btn-primary" href="{{ route('item.index') }}" role="button">Kembali</a>
			</form>
		</div>
	</div>
</div>
@endsection
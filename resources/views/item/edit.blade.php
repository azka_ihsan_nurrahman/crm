@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Edit Data Barang</h3>
			<form action="{{ route('item.update', $item) }}" method="post">
				
				{{ csrf_field() }}
				{{ method_field('put') }}

				<div class="form-group">
					<label>Nama Barang</label>
					<input type="text" name="item_name" class="form-control" value="{{ $item->item_name}}">
				</div>
				<div class="form-group">
					<label>Harga</label>
					<input type="number" name="price" min="0" class="form-control" value="{{ $item->price}}">
				</div>
				<div class="form-group">
					<label>Stok</label>
					<input type="number" name="stock" min="1" class="form-control" value="{{ $item->stock}}">
				</div>
				<div class="form-group">
					<label>Deskripsi Barang</label>
					<input type="textarea" name="description" class="form-control" value="{{ $item->description}}">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
				<a class="btn btn-primary" href="{{ route('item.index') }}" role="button">Kembali</a>
			</form>
		</div>
	</div>
</div>
@endsection
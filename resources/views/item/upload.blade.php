@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Upload Gambar Barang</h3>
			<form action="{{ route('store.picture', $item->id) }}" enctype="multipart/form-data" method="post">
				
				{{ csrf_field() }}
				{{ method_field('put') }}

				<input type="file" name="image" class="custom-file-input">
				<br>
				<button type="submit" class="btn btn-primary">Upload</button>
				<a class="btn btn-primary" href="{{ route('item.index') }}" role="button">Kembali</a>
			</form>
		</div>
	</div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Edit Data Loyalty Program</h3>
			<form action="{{ route('loyaltyprogram.update', $loyaltyprogram) }}" method="post">
				
				{{ csrf_field() }}
				{{ method_field('put') }}

				<div class="form-group">
					<label>Nama Program</label>
					<input type="text" name="program_name" class="form-control" value="{{ $loyaltyprogram->program_name}}">
				</div>
				<div class="form-group">
					<label>Tanggal Mulai</label>
					<input type="date" name="start_date" class="form-control" value="{{ $loyaltyprogram->start_date}}">
				</div>
				<div class="form-group">
					<label>Tanggal Berakhir</label>
					<input type="date" name="end_date" class="form-control" value="{{ $loyaltyprogram->end_date}}">
				</div>
				<div class="form-group">
					<label>Diskon (%)</label>
					<input type="number" name="discount" min="0" max="100" class="form-control" value="{{ $loyaltyprogram->discount}}">
				</div>
				<div class="form-group">
					<label>Deskripsi Program</label>
					<input type="textarea" name="description" class="form-control" value="{{ $loyaltyprogram->description}}">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
				<a class="btn btn-primary" href="{{ route('loyaltyprogram.index') }}" role="button">Kembali</a>
			</form>
		</div>
	</div>
</div>
@endsection
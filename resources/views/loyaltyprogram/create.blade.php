@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Tambah Loyalty Program Baru</h3>
			@if ($errors->count() > 0)
			<div class="alert alert-danger">
				@foreach ($errors->all() as $message)
				    <li>{{ $message }}</li>
				@endforeach
			</div>
			@endif
			<form action="{{ route('loyaltyprogram.store') }}" method="post">
				
				{{ csrf_field() }}

				<div class="form-group">
					<label>Nama Program</label>
					<input type="text" name="program_name" class="form-control" value="{{ old('program_name') }}">
				</div>
				<div class="form-group">
					<label>Tanggal Mulai</label>
					<input type="date" name="start_date" class="form-control" value="{{ old('start_date') }}">
				</div>
				<div class="form-group">
					<label>Tanggal Berakhir</label>
					<input type="date" name="end_date" class="form-control" value="{{ old('end_date') }}">
				</div>
				<div class="form-group">
					<label>Diskon (%)</label>
					<input type="number" name="discount" min="0" max="100" class="form-control" value="{{ old('discount') }}">
				</div>
				<div class="form-group">
					<label>Deskripsi Program</label>
					<input type="textarea" name="description" class="form-control" value="{{ old('description') }}">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
				<a class="btn btn-primary" href="{{ route('loyaltyprogram.index') }}" role="button">Kembali</a>
			</form>
		</div>
	</div>
</div>
@endsection
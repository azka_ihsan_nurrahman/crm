@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Daftar Loyalty Program</h3>
			@if(Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<a href="{{ route('loyaltyprogram.create') }}" class="btn btn-primary">Tambah Program</a>
			<br></br>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama Program</th>
						<th>Tanggal Mulai</th>
						<th>Tanggal Berakhir</th>			
						<th>Diskon</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($loyaltyprograms as $loyaltyprogram)
					<tr>
						<td>{{ $loyaltyprogram->id }}</td>
						<td>{{ $loyaltyprogram->program_name }}</td>
						<td>{{ date('d-m-Y', strtotime($loyaltyprogram->start_date)) }}</td>
						<td>{{ date('d-m-Y', strtotime($loyaltyprogram->end_date)) }}</td>
						<td>{{ $loyaltyprogram->discount }} %</td>
						<td>
							<a href="{{ route('loyaltyprogram.show', $loyaltyprogram->id) }}" class="btn btn-warning">View</a>
							<a href="{{ route('loyaltyprogram.edit', $loyaltyprogram->id) }}" class="btn btn-primary">Edit</a>
							<a href="{{ route('loyaltyprogram.delete', $loyaltyprogram->id) }}" class="btn btn-danger">Delete</a>
						</td>
					</tr>
					@empty
					    <td colspan="6">Belum ada data</td>
					@endforelse
				</tbody>
			</table>
			{!! $loyaltyprograms->links() !!}
		</div>
	</div>
</div>
@endsection
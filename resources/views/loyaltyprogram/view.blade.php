@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Detail Data Loyalty Program</h3>
				<div class="form-group">
					<div class="col-lg-3 control-label">Nama Program</div>
					<div class="col-lg-9"> {{ $loyaltyprogram->program_name}} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Tanggal Mulai</div>
					<div class="col-lg-9"> {{ date('d-m-Y', strtotime($loyaltyprogram->start_date)) }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Tanggal Berakhir</div>
					<div class="col-lg-9"> {{ date('d-m-Y', strtotime($loyaltyprogram->end_date)) }} </div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Diskon</div>
					<div class="col-lg-9"> {{ $loyaltyprogram->discount}} %</div>
				</div>
				<div class="form-group">
					<div class="col-lg-3 control-label">Deskripsi</div>
					<div class="col-lg-9"> {{ $loyaltyprogram->description}} </div>
				</div>
				<br>
				<a class="btn btn-primary" href="{{ route('loyaltyprogram.index') }}" role="button">Kembali</a>
		</div>
	</div>
</div>
@endsection

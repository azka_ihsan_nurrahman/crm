@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Laporan Omzet</h3>
			@if ($time == "-1 year")
			    <h4>Tahunan</h4>
			@elseif ($time == "-3 month")
			    <h4>Kuartal</h4>
			@elseif ($time == "-1 month")
			    <h4>Bulanan</h4>
			@elseif ($time == "-1 week")
				<h4>Mingguan</h4>
			@elseif ($time == "yesterday")
				<h4>Harian</h4>
			@endif
			@if(Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Customer</th>
						<th>Shopkeeper</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					@php
					$nomor = 1;
					@endphp
					@forelse ($bills as $bill)
					<tr>
						<td>{{ $nomor }}</td>
						<td>{{ $bill->customer->customer_name }}</td>
						<td>{{ $bill->shopkeeper->shopkeeper_name }}</td>
						<td>Rp {{ number_format($bill->total, 2, ',', '.') }}</td>
					</tr>
					@php
					$nomor = $nomor + 1;
					@endphp
					@empty
					    <td colspan="4">Belum ada data</td>
					@endforelse
					<tr>
						<td colspan="4">
							<a href="{{ route('omzetreport.printreportdetail', $time) }}" class="btn btn-warning">Cetak Laporan</a>
							<a href="{{ route('omzetreport.index') }}" class="btn btn-primary">Kembali</a>
						</td>
					</tr>
				</tbody>
			</table>
			{!! $bills->links() !!}
		</div>
	</div>
	<div class="row">
		<div class="col">
			
		</div>
	</div>
</div>
@endsection
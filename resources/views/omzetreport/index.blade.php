@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Laporan Omzet</h3>
			@if(Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Waktu</th>
						<th>Total Omzet</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><a href="{{ route('omzetreport.show', '-1 year') }}" >Omzet 1 Tahun Terakhir</a></td>
						<td>Rp {{ number_format($omzetyear, 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td><a href="{{ route('omzetreport.show', '-3 month') }}" >Omzet 1 Kuartal Terakhir</a></td>
						<td>Rp {{ number_format($omzetquarter, 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td><a href="{{ route('omzetreport.show', '-1 month') }}" >Omzet 1 Bulan Terakhir</a></td>
						<td>Rp {{ number_format($omzetmonth, 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td><a href="{{ route('omzetreport.show', '-1 week') }}" >Omzet 1 Minggu Terakhir</a></td>
						<td>Rp {{ number_format($omzetweek, 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td><a href="{{ route('omzetreport.show', 'yesterday') }}" >Omzet 1 Hari Terakhir</a></td>
						<td>Rp {{ number_format($omzetday, 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td><b>Total Omzet Keseluruhan</b></td>
						<td><b>Rp {{ number_format($totalomzet, 2, ',', '.') }}</b></td>
					</tr>
					<tr>
						<td colspan="2">
							<a href="{{ route('omzetreport.printomzetreport') }}" class="btn btn-warning">Cetak Laporan</a>
							<a href="{{ route('home') }}" class="btn btn-primary">Kembali</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col">
			
		</div>
	</div>
</div>
@endsection
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>Laporan Omzet</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col">
			<h3 align="center">Laporan Omzet</h3>
			<h4 align="center">CRM-System</h4>
			<h4 align="center">Detail Per Periode Waktu</h4>
			@if ($time == "-1 year")
			    <h4 align="center">Tahunan</h4>
			@elseif ($time == "-3 month")
			    <h4 align="center">Kuartal</h4>
			@elseif ($time == "-1 month")
			    <h4 align="center">Bulanan</h4>
			@elseif ($time == "-1 week")
				<h4 align="center">Mingguan</h4>
			@elseif ($time == "yesterday")
				<h4 align="center">Harian</h4>
			@endif
			<table border="3">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Customer</th>
						<th>Shopkeeper</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					@php
					$nomor = 1;
					@endphp
					@forelse ($bills as $bill)
					<tr>
						<td>{{ $nomor }}</td>
						<td>{{ $bill->customer->customer_name }}</td>
						<td>{{ $bill->shopkeeper->shopkeeper_name }}</td>
						<td>Rp {{ number_format($bill->total, 2, ',', '.') }}</td>
					</tr>
					@php
					$nomor = $nomor + 1;
					@endphp
					@empty
					    <td colspan="4">Belum ada data</td>
					@endforelse
				</tbody>
			</table>
			<h4 align="right">
				@php
				echo date('d-m-Y H:i:s');
				@endphp
			</h4>
		</div>
	</div>
</div>
</body>
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>Laporan Omzet</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col">
			<h3 align="center">Laporan Omzet</h3>
			<h4 align="center">CRM-System</h4>
			<h4 align="center">Per Periode Waktu</h4>
			<table border="3">
				<thead>
					<tr>
						<th>Waktu</th>
						<th>Total Omzet</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Omzet 1 Tahun Terakhir</td>
						<td>Rp {{ number_format($omzetyear, 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td>Omzet 1 Kuartal Terakhir</td>
						<td>Rp {{ number_format($omzetquarter, 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td>Omzet 1 Bulan Terakhir</td>
						<td>Rp {{ number_format($omzetmonth, 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td>Omzet 1 Minggu Terakhir</td>
						<td>Rp {{ number_format($omzetweek, 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td>Omzet 1 Hari Terakhir</td>
						<td>Rp {{ number_format($omzetday, 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td><b>Total Omzet Keseluruhan</b></td>
						<td><b>Rp {{ number_format($totalomzet, 2, ',', '.') }}</b></td>
					</tr>
				</tbody>
			</table>
			<h4 align="right">
				@php
				echo date('d-m-Y H:i:s');
				@endphp
			</h4>
		</div>
	</div>
</div>
</body>
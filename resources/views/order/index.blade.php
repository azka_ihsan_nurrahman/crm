@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Entri Order</h3>
			@if(Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<a href="{{ route('order.create') }}" class="btn btn-primary">Tambah Order</a>
			<br></br>
			<form action="{{ route('order.index') }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('GET') }}
					<div class="form-group">
							<label>Customer</label>
							<select class="form-control" name="customer_id" onchange="this.form.submit()">
								<option value="none" selected disabled>Pilih Customer</option>
						    	@foreach($customers as $customer)
						    		<option value="{{ $customer->id }}">{{ $customer->customer_name }}</option>
						    	@endforeach
							</select>
					</div>    
			</form>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama Barang</th>
						<th>Satuan</th>
						<th>Harga</th>
						<th>Diskon</th>			
						<th>Subtotal</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					@php
					$total = 0;
					$nomor = 1;
					@endphp
					@forelse ($orders as $order)
					@php
					$total = $total + $order->subtotal;
					@endphp
					<tr>
						<td>{{ $nomor }}</td>
						<td>{{ $order->item->item_name }}</td>
						<td>{{ $order->quantity }}</td>
						<td>Rp {{ number_format($order->item->price, 2, ',', '.') }}</td>
						<td>{{ $order->loyaltyprogram->discount }} %</td>
						<td>Rp {{ number_format($order->subtotal, 2, ',', '.') }}</td>
						<td>
							<a href="{{ route('order.edit', $order->id) }}" class="btn btn-primary">Edit</a>
							<a href="{{ route('order.delete', $order->id) }}" class="btn btn-danger">Delete</a>
						</td>
					</tr>
					@php
					$nomor = $nomor + 1;
					@endphp
					@empty
					    <td colspan="7">Belum ada data</td>
					@endforelse
					<tr>
						<td colspan="5"><b>Total</b></td>
						<td><b>Rp {{ number_format($total, 2, ',', '.') }}</b></td>
						<td>
							<a href="{{ route('bill.index') }}" class="btn btn-warning">Checkout</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col">
			
		</div>
	</div>
</div>
@endsection
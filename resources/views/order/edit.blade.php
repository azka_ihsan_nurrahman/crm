@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Edit Data Order</h3>
			<form action="{{ route('order.update', $order->id) }}" method="post">
				
				{{ csrf_field() }}
				{{ method_field('put') }}

				<div class="form-group">
					<label>Nama Barang</label>
					<input list="item_name" name="item_name" class="form-control" value="{{ $itemorder->item_name }}">
					  <datalist id="item_name">
					  	@foreach ($items as $item)
					    <option value="{{ $item->item_name }}"> 
					    @endforeach
					  </datalist>
				</div>
				<div class="form-group">
					<label>Jumlah</label>
					<input type="text" name="quantity" class="form-control" value="{{ $order->quantity }}">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
				<a class="btn btn-primary" href="{{ route('order.index') }}" role="button">Kembali</a>
			</form>
		</div>
	</div>
</div>
@endsection

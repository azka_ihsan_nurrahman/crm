@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Tambah Order</h3>
			@if ($errors->count() > 0)
			<div class="alert alert-danger">
				@foreach ($errors->all() as $message)
				    <li>{{ $message }}</li>
				@endforeach
			</div>
			@endif
			<form action="{{ route('order.store') }}" method="post">
				
				{{ csrf_field() }}

				<div class="form-group">
					<label>Nama Customer</label>
					  <select name="customer_name" class="form-control">
					  	@foreach ($customers as $customer)
					    <option value="{{ $customer->customer_name }}"> {{ $customer->customer_name }} </option>
					    @endforeach
					  </select>
				</div>
				<div class="form-group">
					<label>Nama Barang</label>
					<input list="item_name" name="item_name" class="form-control">
					  <datalist id="item_name">
					  	@foreach ($items as $item)
					    <option value="{{ $item->item_name }}"> 
					    @endforeach
					  </datalist>
				</div>
				<div class="form-group">
					<label>Jumlah</label>
					<input type="text" name="quantity" class="form-control" value="{{ old('quantity') }}">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
				<a class="btn btn-primary" href="{{ route('order.index') }}" role="button">Kembali</a>
			</form>
		</div>
	</div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Laporan Keuangan & Pembayaran</h3>
			@if(Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Metode Pembayaran</th>
						<th>Total Customer</th>
						<th>Total Shopkeeper</th>
						<th>Jumlah Transaksi</th>
						<th>Total Transaksi</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td><a href="{{ route('financereport.cashindex') }}">Cash</a></td>
						<td>{{ $datacash['countcashcustomer'] }}</td>
						<td>{{ $datacash['countcashshopkeeper'] }}</td>
						<td>{{ $datacash['countcash'] }}</td>
						<td>Rp {{ number_format($datacash['totalcash'], 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td>2</td>
						<td><a href="{{ route('financereport.debitindex') }}">Debit</a></td>
						<td>{{ $datadebit['countdebitcustomer'] }}</td>
						<td>{{ $datadebit['countdebitshopkeeper'] }}</td>
						<td>{{ $datadebit['countdebit'] }}</td>
						<td>Rp {{ number_format($datadebit['totaldebit'], 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td>3</td>
						<td><a href="{{ route('financereport.creditindex') }}">Credit</a></td>
						<td>{{ $datacredit['countcreditcustomer'] }}</td>
						<td>{{ $datacredit['countcreditshopkeeper'] }}</td>
						<td>{{ $datacredit['countcredit'] }}</td>
						<td>Rp {{ number_format($datacredit['totalcredit'], 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td colspan="4"><b>Total</b></td>
						<td><b>{{ $datacash['countcash'] + $datadebit['countdebit'] + $datacredit['countcredit'] }}</b></td>
						<td><b>Rp {{ number_format(($datacash['totalcash'] + $datadebit['totaldebit'] + $datacredit['totalcredit']), 2, ',', '.') }}</b></td>
					</tr>
					<tr>
						<td colspan="6">
							<a href="{{ route('financereport.printreport') }}" class="btn btn-warning">Cetak Laporan</a>
							<a href="{{ route('home') }}" class="btn btn-primary">Kembali</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col">
			
		</div>
	</div>
</div>
@endsection
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>Laporan Keuangan & Pembayaran</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col">
			<h3 align="center">Laporan Keuangan & Pembayaran</h3>
			<h4 align="center">CRM-System</h4>
			<table border="3">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Metode Pembayaran</th>
						<th>Total Customer</th>
						<th>Total Shopkeeper</th>
						<th>Jumlah Transaksi</th>
						<th>Total Transaksi</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Cash</td>
						<td>{{ $datacash['countcashcustomer'] }}</td>
						<td>{{ $datacash['countcashshopkeeper'] }}</td>
						<td>{{ $datacash['countcash'] }}</td>
						<td>Rp {{ number_format($datacash['totalcash'], 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Debit</td>
						<td>{{ $datadebit['countdebitcustomer'] }}</td>
						<td>{{ $datadebit['countdebitshopkeeper'] }}</td>
						<td>{{ $datadebit['countdebit'] }}</td>
						<td>Rp {{ number_format($datadebit['totaldebit'], 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Credit</td>
						<td>{{ $datacredit['countcreditcustomer'] }}</td>
						<td>{{ $datacredit['countcreditshopkeeper'] }}</td>
						<td>{{ $datacredit['countcredit'] }}</td>
						<td>Rp {{ number_format($datacredit['totalcredit'], 2, ',', '.') }}</td>
					</tr>
					<tr>
						<td colspan="4"><b>Total</b></td>
						<td><b>{{ $datacash['countcash'] + $datadebit['countdebit'] + $datacredit['countcredit'] }}</b></td>
						<td><b>Rp {{ number_format(($datacash['totalcash'] + $datadebit['countdebit'] + $datacredit['totalcredit']), 2, ',', '.') }}</b></td>
					</tr>
				</tbody>
			</table>
			<h4 align="right">
				@php
				echo date('d-m-Y H:i:s');
				@endphp
			</h4>
		</div>
	</div>
</div>
</body>
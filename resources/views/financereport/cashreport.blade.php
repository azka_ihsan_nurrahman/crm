<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>Laporan Keuangan & Pembayaran</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col">
			<h3 align="center">Laporan Keuangan & Pembayaran</h3>
			<h4 align="center">CRM-System</h4>
			<h4 align="center">Metode Pembayaran Cash</h4>
			<table border="3">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Customer</th>
						<th>Shopkeeper</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					@php
					$total = 0;
					$nomor = 1;
					@endphp
					@forelse ($billscash as $billcash)
					@php
					$total = $total + $billcash->total;
					@endphp
					<tr>
						<td>{{ $nomor }}</td>
						<td>{{ $billcash->customer->customer_name }}</td>
						<td>{{ $billcash->shopkeeper->shopkeeper_name }}</td>
						<td>Rp {{ number_format($billcash->total, 2, ',', '.') }}</td>
					</tr>
					@php
					$nomor = $nomor + 1;
					@endphp
					@empty
					    <td colspan="4">Belum ada data</td>
					@endforelse
					<tr>
						<td colspan="3"><b>Total</b></td>
						<td><b>Rp {{ number_format($total, 2, ',', '.') }}</b></td>
					</tr>
				</tbody>
			</table>
			<h4 align="right">
				@php
				echo date('d-m-Y H:i:s');
				@endphp
			</h4>
		</div>
	</div>
</div>
</body>
@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Laporan Keuangan & Pembayaran</h3>
			<h4>Debit</h4>
			@if(Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Customer</th>
						<th>Shopkeeper</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					@php
					$nomor = 1;
					@endphp
					@forelse ($billsdebit as $billdebit)
					<tr>
						<td>{{ $nomor }}</td>
						<td>{{ $billdebit->customer->customer_name }}</td>
						<td>{{ $billdebit->shopkeeper->shopkeeper_name }}</td>
						<td>Rp {{ number_format($billdebit->total, 2, ',', '.') }}</td>
					</tr>
					@php
					$nomor = $nomor + 1;
					@endphp
					@empty
					    <td colspan="4">Belum ada data</td>
					@endforelse
					<tr>
						<td colspan="4">
							<a href="{{ route('financereport.printdebitreport') }}" class="btn btn-warning">Cetak Laporan</a>
							<a href="{{ route('financereport.index') }}" class="btn btn-primary">Kembali</a>
						</td>
					</tr>
				</tbody>
			</table>
			{!! $billsdebit->links() !!}
		</div>
	</div>
	<div class="row">
		<div class="col">
			
		</div>
	</div>
</div>
@endsection
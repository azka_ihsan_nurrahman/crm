@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<h3>Checkout</h3>
			@if(Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<form action="{{ route('bill.index') }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('GET') }}
					<div class="form-group">
							<label>Customer</label>
							<select class="form-control" name="customer_id">
						    	@foreach($customers as $customer)
						    		<option value="{{ $customer->id }}">{{ $customer->customer_name }}</option>
						    	@endforeach
							</select>
							<br>
					    	<button type="submit" class="btn btn-primary">Cari</button>
					</div>    
			</form>
			<br>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama Barang</th>
						<th>Satuan</th>
						<th>Harga</th>
						<th>Diskon</th>			
						<th>Subtotal</th>
					</tr>
				</thead>
				<tbody>
					@php
					$total = 0;
					$nomor = 1;
					@endphp
					@forelse ($orders as $order)
					@php
					$total = $total + $order->subtotal;
					@endphp
					<tr>
						<td>{{ $nomor }}</td>
						<td>{{ $order->item->item_name }}</td>
						<td>{{ $order->quantity }}</td>
						<td>Rp {{ number_format($order->item->price, 2, ',', '.') }}</td>
						<td>{{ $order->loyaltyprogram->discount }} %</td>
						<td>Rp {{ number_format($order->subtotal, 2, ',', '.') }}</td>
					</tr>
					@php
					$nomor = $nomor + 1;
					@endphp
					@empty
					    <td colspan="6">Belum ada data</td>
					@endforelse
					<tr>
						<td colspan="5"><b>Total</b></td>
						<td><b>Rp {{ number_format($total, 2, ',', '.') }}</b></td>
					</tr>
					<tr>
						<td colspan="6">
							<form action="{{ route('bill.checkout') }}" method="post" target="_blank">
							{{ csrf_field() }}
							<input type="number" name="total" value="{{ $total }}" hidden>
							<div class="form-group">
								<label>Customer</label>
								<select class="form-control" name="customer_id">
							    	@foreach($customers as $customer)
							    		<option value="{{ $customer->id }}">{{ $customer->customer_name }}</option>
							    	@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Shopkeeper</label>
								<select class="form-control" name="shopkeeper_id">
							    	@foreach($shopkeepers as $shopkeeper)
							    		<option value="{{ $shopkeeper->id }}">{{ $shopkeeper->shopkeeper_name }}</option>
							    	@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Metode Pembayaran</label>
								<select class="form-control" name="payment_method">
							    	<option value="Cash">Cash</option>
							    	<option value="Debit">Debit</option>
							    	<option value="Credit">Credit</option>
								</select>
							</div>
							<div class="form-group" style="float:right;">
								<a href="{{ route('order.index') }}" class="btn btn-primary">Kembali</a>
								<button type="submit" class="btn btn-warning">Bayar dan Cetak Struk</button>
							</div>
							</form>
						</td>	
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col">
			
		</div>
	</div>
</div>
@endsection
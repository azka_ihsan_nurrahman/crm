<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>Struk Pembayaran</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col">
			<h3 align="center">Struk Pembayaran</h3>
			<h4 align="center">CRM-System</h4>
			<h4 align="center">Customer : {{ $bill->customer->customer_name }}</h4>
			<table border="3">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama Barang</th>
						<th>Satuan</th>
						<th>Harga</th>
						<th>Diskon</th>			
						<th>Subtotal</th>
					</tr>
				</thead>
				<tbody>
					@php
					$total = 0;
					$nomor = 1;
					@endphp
					@forelse ($orders as $order)
					@php
					$total = $total + $order->subtotal;
					@endphp
					<tr>
						<td>{{ $nomor }}</td>
						<td>{{ $order->item->item_name }}</td>
						<td>{{ $order->quantity }}</td>
						<td>Rp {{ number_format($order->item->price, 2, ',', '.') }}</td>
						<td>{{ $order->loyaltyprogram->discount }} %</td>
						<td>Rp {{ number_format($order->subtotal, 2, ',', '.') }}</td>
					</tr>
					@php
					$nomor = $nomor + 1;
					@endphp
					@empty
					    <td colspan="6">Belum ada data</td>
					@endforelse
					<tr>
						<td colspan="5"><b>Total</b></td>
						<td><b>Rp {{ number_format($total, 2, ',', '.') }}</b></td>
					</tr>
				</tbody>
			</table>
			<h4>Metode Pembayaran : {{ $bill->payment_method }}</h4>
			<h4>Kasir : {{ $bill->shopkeeper->shopkeeper_name }}</h4>
			<h4 align="right">
				@php
				echo date('d-m-Y H:i:s');
				@endphp
			</h4>
		</div>
	</div>
</div>
</body>
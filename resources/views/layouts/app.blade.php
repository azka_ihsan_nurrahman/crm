<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CRM-System') }}</title>
    <link rel="icon" href="{!! asset('img/icon.png') !!}"/>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    @if (Route::has('login'))
                    @auth
                    @if (Auth::user()->account_level == 4)
                    <ul class="nav navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                              Master Data
                            </a>
                              <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('item.index') }}">Barang</a></li>
                                <li><a class="dropdown-item" href="{{ route('customer.index') }}">Customer</a></li>
                                <li><a class="dropdown-item" href="{{ route('loyaltyprogram.index') }}">Loyalty Program</a></li>
                                <li><a class="dropdown-item" href="{{ route('shopkeeper.index') }}">Shopkeeper</a></li>
                              </ul>
                        </li>
                        <li><a href="{{ route('accesslog.index') }}" class="nav-link">Log Akses</a></li>
                        <li><a href="{{ route('useraccess.index') }}" class="nav-link">Pengaturan Akses</a></li>
                    </ul>
                    @endif
                    @if (Auth::user()->account_level == 3)
                    <ul class="nav navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                              Laporan
                            </a>
                              <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('trendreport.index') }}">Trend</a></li>
                                <li><a class="dropdown-item" href="{{ route('financereport.index') }}">Keuangan</a></li>
                                <li><a class="dropdown-item" href="{{ route('omzetreport.index') }}">Omzet</a></li>
                                <li><a class="dropdown-item" href="{{ route('customerreport.index') }}">Customer Profile</a></li>
                              </ul>
                        </li>
                    </ul>
                    @endif
                    @if (Auth::user()->account_level == 2)
                    <ul class="nav navbar-nav mr-auto">
                        <li><a href="{{ route('order.index') }}" class="nav-link">Entri Transaksi</a></li>
                        <li><a href="{{ route('bill.index') }}" class="nav-link">Konfirmasi Pembayaran</a></li>
                    </ul>
                    @endif
                    @else
                    @endauth
                    @endif

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

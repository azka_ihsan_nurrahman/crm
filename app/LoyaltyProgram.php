<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoyaltyProgram extends Model
{
    protected $fillable = ['program_name', 'start_date', 'end_date', 'discount', 'description'];

    public function orders()
    {
        return $this->hasMany(Order::class, 'loyalty_program_id');
    }
    public function customers()
    {
        return $this->hasMany(Customer::class, 'loyalty_program_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    
    public function user() 
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
    public function loyaltyprogram() 
    {
    	return $this->belongsTo(LoyaltyProgram::class, 'loyalty_program_id');
    }
    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id');
    }
    public function bills()
    {
        return $this->hasMany(Bill::class, 'customer_id');
    }
}

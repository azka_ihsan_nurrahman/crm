<?php

namespace App\Services;

use PDF;
use App\Bill;
use App\Customer;
use App\Item;
use App\LoyaltyProgram;
use App\Order;
use App\Shopkeeper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardService
{
	public function showoverallfinance()
	{
		$bills = Bill::get();
		$totalcash = 0;
		$totaldebit = 0;
		$totalcredit = 0;
		$totaltransaction = 0;
		foreach ($bills as $bill) {
			if ($bill->payment_method == "Cash") {
				$totalcash = $totalcash + $bill->total;
			} else if ($bill->payment_method == "Debit") {
				$totaldebit = $totaldebit + $bill->total;
			} else if ($bill->payment_method == "Credit") {
				$totalcredit = $totalcredit + $bill->total;
			}
			$totaltransaction = $totaltransaction + 1;
		}
		$totalomzet = $totalcash + $totaldebit + $totalcredit;
		$financedata = array("totalcash" => $totalcash, "totaldebit" => $totaldebit, "totalcredit" => $totalcredit, "totaltransaction" => $totaltransaction, "totalomzet" => $totalomzet);
		return $financedata;
	}
	public function showoveralltrend()
	{
    	$customerrank = array();
    	$customerrankmax = array();
    	$ranktable = 1;
    	$customers = Customer::withCount('bills')->get();
		foreach ($customers as $customer) {
			$customerrank[$ranktable]['name'] = $customer->customer_name;
		    $customerrank[$ranktable]['count'] = $customer->bills_count;
		    $ranktable++;
		}
		foreach ($customerrank as $key) {
			if ($key['count'] == max(array_column($customerrank, 'count'))) {
        		$customerrankmax = $key;
        	}
		}
		$loyaltyprogramrank = array();
		$loyaltyprogramrankmax = array();
		$ranktable = 1;
		$loyaltyprograms = LoyaltyProgram::withCount('orders')->get();
		foreach ($loyaltyprograms as $loyaltyprogram) {
			$loyaltyprogramrank[$ranktable]['name'] = $loyaltyprogram->program_name;
		    $loyaltyprogramrank[$ranktable]['count'] = $loyaltyprogram->orders_count;
		    $ranktable++;
		}
		foreach ($loyaltyprogramrank as $key) {
			if ($key['count'] == max(array_column($loyaltyprogramrank, 'count'))) {
        		$loyaltyprogramrankmax = $key;
        	}
		}
		$shopkeeperrank = array();
		$shopkeeperrankmax = array();
		$ranktable = 1;
		$shopkeepers = Shopkeeper::withCount('bills')->get();
		foreach ($shopkeepers as $shopkeeper) {
			$shopkeeperrank[$ranktable]['name'] = $shopkeeper->shopkeeper_name;
		    $shopkeeperrank[$ranktable]['count'] = $shopkeeper->bills_count;
		    $ranktable++;
		}
		foreach ($shopkeeperrank as $key) {
			if ($key['count'] == max(array_column($shopkeeperrank, 'count'))) {
        		$shopkeeperrankmax = $key;
        	}
		}
		$itemrank = array();
		$itemrankmax = array();
		$ranktable = 1;
		$items = Item::get();
		foreach ($items as $item) {
			$itemrank[$ranktable]['name'] = $item->item_name;
			$orders = Order::where('item_id', $item->id)->get();
			foreach ($orders as $order) {
				if (!isset($itemrank[$ranktable]['count'])) {
					$itemrank[$ranktable]['count'] = $order->quantity;
				} else {
					$itemrank[$ranktable]['count'] = $itemrank[$ranktable]['count'] + $order->quantity;
				}
			}			
		    $ranktable++;
		}
		foreach ($itemrank as $key) {
			if (!isset($key['count'])) {
				$itemrankmax = 1;
			} else if ($key['count'] == max(array_column($itemrank, 'count'))) {
        		$itemrankmax = $key;
        	}
		}
		$trenddata = array("customerrankmax" => $customerrankmax, "loyaltyprogramrankmax" => $loyaltyprogramrankmax, "shopkeeperrankmax" => $shopkeeperrankmax, "itemrankmax" => $itemrankmax);
		return $trenddata;
	}
	public function showoverallcustomer()
	{
    	$malecustomers = Customer::where('gender', 'Pria')->count();
    	$femalecustomers = Customer::where('gender', 'Wanita')->count();
    	$marriedcustomers = Customer::where('marital_status', 'Sudah Menikah')->count();
    	$singlecustomers = Customer::where('marital_status', 'Belum Menikah')->count();
    	$childcustomers = Customer::whereBetween('age', [0, 13])->count();
    	$teencustomers = Customer::whereBetween('age', [14, 21])->count();
    	$productivecustomers = Customer::whereBetween('age', [22, 40])->count();
    	$adultcustomers = Customer::whereBetween('age', [41, 60])->count();
    	$eldercustomers = Customer::whereBetween('age', [61, 1000])->count();
    	$customerprofiledata = array('malecustomers' => $malecustomers, 'femalecustomers' => $femalecustomers, 'marriedcustomers' => $marriedcustomers, 'singlecustomers' => $singlecustomers, 'childcustomers' => $childcustomers, 'teencustomers' => $teencustomers, 'productivecustomers' => $productivecustomers, 'adultcustomers' => $adultcustomers, 'eldercustomers' => $eldercustomers);
    	return $customerprofiledata;
	}
}
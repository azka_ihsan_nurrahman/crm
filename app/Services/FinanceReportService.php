<?php

namespace App\Services;

use PDF;
use App\Bill;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FinanceReportService
{
	public function showcash()
	{
		$billscash = Bill::where('payment_method' , 'Cash')->get();
		$totalcash = 0;
		$countcash = 0;
		$countcashcustomer = 0;
		$countcashcustomerarray = array();
		$countcashshopkeeper = 0;
		$countcashshopkeeperarray = array();
		foreach ($billscash as $billcash) {
			$totalcash = $totalcash + $billcash->total;
			$countcash = $countcash + 1;
			array_push($countcashcustomerarray, $billcash->customer_id);
			array_push($countcashshopkeeperarray, $billcash->shopkeeper_id);
		}
		$countcashcustomer = count(array_unique($countcashcustomerarray));
		$countcashshopkeeper = count(array_unique($countcashshopkeeperarray));
		$datacash = array("totalcash" => $totalcash, "countcash" => $countcash, "countcashcustomer" => $countcashcustomer, "countcashshopkeeper" => $countcashshopkeeper);
		return $datacash;
	}
	public function showdebit()
	{
		$billsdebit = Bill::where('payment_method' , 'Debit')->get();
		$totaldebit = 0;
		$countdebit = 0;
		$countdebitcustomer = 0;
		$countdebitcustomerarray = array();
		$countdebitshopkeeper = 0;
		$countdebitshopkeeperarray = array();
		foreach ($billsdebit as $billdebit) {
			$totaldebit = $totaldebit + $billdebit->total;
			$countdebit = $countdebit + 1;
			array_push($countdebitcustomerarray, $billdebit->customer_id);
			array_push($countdebitshopkeeperarray, $billdebit->shopkeeper_id);
		}
		$countdebitcustomer = count(array_unique($countdebitcustomerarray));
		$countdebitshopkeeper = count(array_unique($countdebitshopkeeperarray));
		$datadebit = array("totaldebit" => $totaldebit, "countdebit" => $countdebit, "countdebitcustomer" => $countdebitcustomer, "countdebitshopkeeper" => $countdebitshopkeeper);
		return $datadebit;
	}
	public function showcredit()
	{
		$billscredit = Bill::where('payment_method' , 'Credit')->get();
		$totalcredit = 0;
		$countcredit = 0;
		$countcreditcustomer = 0;
		$countcreditcustomerarray = array();
		$countcreditshopkeeper = 0;
		$countcreditshopkeeperarray = array();
		foreach ($billscredit as $billcredit) {
			$totalcredit = $totalcredit + $billcredit->total;
			$countcredit = $countcredit + 1;
			array_push($countcreditcustomerarray, $billcredit->customer_id);
			array_push($countcreditshopkeeperarray, $billcredit->shopkeeper_id);
		}
		$countcreditcustomer = count(array_unique($countcreditcustomerarray));
		$countcreditshopkeeper = count(array_unique($countcreditshopkeeperarray));
		$datacredit = array("totalcredit" => $totalcredit, "countcredit" => $countcredit, "countcreditcustomer" => $countcreditcustomer, "countcreditshopkeeper" => $countcreditshopkeeper);
		return $datacredit;
	}
}
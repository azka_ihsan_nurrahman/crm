<?php

namespace App\Listeners;

use App\Events\LoginEvent;
use App\Notifications\LoggedInUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Events\Login;

class LoginEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoginEvent  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $login = $event->user;
        $user = $login;
        $user->notify(new LoggedInUser($login));
    }
}

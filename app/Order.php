<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function customer() 
    {
    	return $this->belongsTo(Customer::class, 'customer_id');
    }
    public function item() 
    {
    	return $this->belongsTo(Item::class, 'item_id');
    }
    public function items() 
    {
        return $this->hasOne(Item::class, 'id', 'item_id');
    }      
    public function loyaltyprogram() 
    {
    	return $this->belongsTo(LoyaltyProgram::class, 'loyalty_program_id');
    }
}

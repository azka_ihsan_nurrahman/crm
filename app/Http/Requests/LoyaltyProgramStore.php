<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoyaltyProgramStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'program_name' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'discount' => 'required|digits_between:0,100',
            'description' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'program_name.required' => 'Nama Program Harus Diisi',
            'start_date.required' => 'Tanggal Mulai Harus Diisi',
            'end_date.required' => 'Tanggal Berakhir Harus Diisi',
            'discount.required' => 'Diskon Harus Diisi',
            'description.required' => 'Deskripsi Program Harus Diisi',
            'violator_name.required' => 'Nama Pelanggar Harus Diisi',
            'start_date.date' => 'Tanggal Mulai Harus Berupa Format Tanggal',
            'end_date.date' => 'Tanggal Berakhir Harus Berupa Format Tanggal',
            'end_date.after:start_date' => 'Tanggal Berakhir Harus Setelah Tanggal Mulai',
            'discount.digits_between:0,100' => 'Diskon Harus Berisi Angka di antara 0-100',
        ];
    }
}

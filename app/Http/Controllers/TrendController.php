<?php

namespace App\Http\Controllers;

use PDF;
use App\Bill;
use App\Customer;
use App\Item;
use App\LoyaltyProgram;
use App\Order;
use App\Shopkeeper;
use Illuminate\Http\Request;

class TrendController extends Controller
{
    public function index()
    {
    	$customerrank = array();
    	$customerrankmax = array();
    	$ranktable = 1;
    	$customers = Customer::withCount('bills')->get();
		foreach ($customers as $customer) {
			$customerrank[$ranktable]['name'] = $customer->customer_name;
		    $customerrank[$ranktable]['count'] = $customer->bills_count;
		    $ranktable++;
		}
		foreach ($customerrank as $key) {
			if ($key['count'] == max(array_column($customerrank, 'count'))) {
        		$customerrankmax = $key;
        	}
		}
		$loyaltyprogramrank = array();
		$loyaltyprogramrankmax = array();
		$ranktable = 1;
		$loyaltyprograms = LoyaltyProgram::withCount('orders')->get();
		foreach ($loyaltyprograms as $loyaltyprogram) {
			$loyaltyprogramrank[$ranktable]['name'] = $loyaltyprogram->program_name;
		    $loyaltyprogramrank[$ranktable]['count'] = $loyaltyprogram->orders_count;
		    $ranktable++;
		}
		foreach ($loyaltyprogramrank as $key) {
			if ($key['count'] == max(array_column($loyaltyprogramrank, 'count'))) {
        		$loyaltyprogramrankmax = $key;
        	}
		}
		$shopkeeperrank = array();
		$shopkeeperrankmax = array();
		$ranktable = 1;
		$shopkeepers = Shopkeeper::withCount('bills')->get();
		foreach ($shopkeepers as $shopkeeper) {
			$shopkeeperrank[$ranktable]['name'] = $shopkeeper->shopkeeper_name;
		    $shopkeeperrank[$ranktable]['count'] = $shopkeeper->bills_count;
		    $ranktable++;
		}
		foreach ($shopkeeperrank as $key) {
			if ($key['count'] == max(array_column($shopkeeperrank, 'count'))) {
        		$shopkeeperrankmax = $key;
        	}
		}
		$itemrank = array();
		$itemrankmax = array();
		$ranktable = 1;
		$items = Item::get();
		foreach ($items as $item) {
			$itemrank[$ranktable]['name'] = $item->item_name;
			$orders = Order::where('item_id', $item->id)->get();
			foreach ($orders as $order) {
				if (!isset($itemrank[$ranktable]['count'])) {
					$itemrank[$ranktable]['count'] = $order->quantity;
				} else {
					$itemrank[$ranktable]['count'] = $itemrank[$ranktable]['count'] + $order->quantity;
				}
			}			
		    $ranktable++;
		}
		foreach ($itemrank as $key) {

			if (!isset($key['count'])) {
				$itemrankmax = 1;
			} else if ($key['count'] == max(array_column($itemrank, 'count'))) {
        		$itemrankmax = $key;
        	}
		}
		return view('trendreport.index', ["customerrankmax" => $customerrankmax, "loyaltyprogramrankmax" => $loyaltyprogramrankmax, "shopkeeperrankmax" => $shopkeeperrankmax, "itemrankmax" => $itemrankmax]);
    }
    public function show(Request $request, $group)
    {
    	if ($group == "customer") {
	    	$customerrank = array();
	    	$ranktable = 1;
	    	$customers = Customer::withCount('bills')->get();
			foreach ($customers as $customer) {
				$customerrank[$ranktable]['name'] = $customer->customer_name;
			    $customerrank[$ranktable]['count'] = $customer->bills_count;
			    $ranktable++;
			}
			$data = $customerrank; 
    	} else if ($group == "loyaltyprogram") {
			$loyaltyprogramrank = array();
			$ranktable = 1;
			$loyaltyprograms = LoyaltyProgram::withCount('orders')->get();
			foreach ($loyaltyprograms as $loyaltyprogram) {
				$loyaltyprogramrank[$ranktable]['name'] = $loyaltyprogram->program_name;
			    $loyaltyprogramrank[$ranktable]['count'] = $loyaltyprogram->orders_count;
			    $ranktable++;
			}
			$data = $loyaltyprogramrank;    		
    	} else if ($group == "shopkeeper") {
			$shopkeeperrank = array();
			$ranktable = 1;
			$shopkeepers = Shopkeeper::withCount('bills')->get();
			foreach ($shopkeepers as $shopkeeper) {
				$shopkeeperrank[$ranktable]['name'] = $shopkeeper->shopkeeper_name;
			    $shopkeeperrank[$ranktable]['count'] = $shopkeeper->bills_count;
			    $ranktable++;
			}
			$data = $shopkeeperrank;   		
    	} else if ($group == "item") {
			$itemrank = array();
			$ranktable = 1;
			$items = Item::get();
			foreach ($items as $item) {
				$itemrank[$ranktable]['name'] = $item->item_name;
				$orders = Order::where('item_id', $item->id)->get();
				foreach ($orders as $order) {
					if (!isset($itemrank[$ranktable]['count'])) {
						$itemrank[$ranktable]['count'] = $order->quantity;
					} else {
						$itemrank[$ranktable]['count'] = $itemrank[$ranktable]['count'] + $order->quantity;
					}
				}			
			    $ranktable++;
			}
			$data = $itemrank;
    	}
    	return view('trendreport.view', ['data' => $data, 'group' => $group]);
    }
    public function printtrendreport(Request $request, $group)
    {
    	if ($group == "customer") {
	    	$customerrank = array();
	    	$ranktable = 1;
	    	$customers = Customer::withCount('bills')->get();
			foreach ($customers as $customer) {
				$customerrank[$ranktable]['name'] = $customer->customer_name;
			    $customerrank[$ranktable]['count'] = $customer->bills_count;
			    $ranktable++;
			}
			$data = $customerrank;
			$filename = "customertrendreport.pdf"; 
    	} else if ($group == "loyaltyprogram") {
			$loyaltyprogramrank = array();
			$ranktable = 1;
			$loyaltyprograms = LoyaltyProgram::withCount('orders')->get();
			foreach ($loyaltyprograms as $loyaltyprogram) {
				$loyaltyprogramrank[$ranktable]['name'] = $loyaltyprogram->program_name;
			    $loyaltyprogramrank[$ranktable]['count'] = $loyaltyprogram->orders_count;
			    $ranktable++;
			}
			$data = $loyaltyprogramrank;
			$filename = "loyaltyprogramtrendreport.pdf";    		
    	} else if ($group == "shopkeeper") {
			$shopkeeperrank = array();
			$ranktable = 1;
			$shopkeepers = Shopkeeper::withCount('bills')->get();
			foreach ($shopkeepers as $shopkeeper) {
				$shopkeeperrank[$ranktable]['name'] = $shopkeeper->shopkeeper_name;
			    $shopkeeperrank[$ranktable]['count'] = $shopkeeper->bills_count;
			    $ranktable++;
			}
			$data = $shopkeeperrank;
			$filename = "shopkeepertrendreport.pdf";   		
    	} else if ($group == "item") {
			$itemrank = array();
			$ranktable = 1;
			$items = Item::get();
			foreach ($items as $item) {
				$itemrank[$ranktable]['name'] = $item->item_name;
				$orders = Order::where('item_id', $item->id)->get();
				foreach ($orders as $order) {
					if (!isset($itemrank[$ranktable]['count'])) {
						$itemrank[$ranktable]['count'] = $order->quantity;
					} else {
						$itemrank[$ranktable]['count'] = $itemrank[$ranktable]['count'] + $order->quantity;
					}
				}			
			    $ranktable++;
			}
			$data = $itemrank;
			$filename = "itemtrendreport.pdf";
		}
		$data = ['data' => $data, 'group' => $group];
    	$pdf = PDF::loadView('trendreport.report', $data);
		return $pdf->stream($filename);
    }
}

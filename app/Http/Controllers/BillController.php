<?php

namespace App\Http\Controllers;

use PDF;
use Validator;
use App\Bill;
use App\Customer;
use App\Order;
use App\Shopkeeper;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BillController extends Controller
{
    public function index(Request $request)
    {
        $customer_id = $request->get('customer_id');
        $customers = Customer::get();
        $shopkeepers = Shopkeeper::get();
    	$orders = Order::get()->where('status' , 'PROCESS')->where('customer_id', $customer_id);
        return view('bill.index', ['orders' => $orders, 'customers' => $customers, 'shopkeepers' => $shopkeepers]);
    }
    public function checkout(Request $request)
    {
    	$customers = Customer::get();
    	$customerarray = array();
    	foreach ($customers as $customer) {
    		array_push($customerarray, $customer->id);
    	}
    	$shopkeepers = Shopkeeper::get();
    	$shopkeeperarray = array();
    	foreach ($shopkeepers as $shopkeeper) {
    		array_push($shopkeeperarray, $shopkeeper->id);
    	}
        $rules = [
        	'customer_id' => 'required|in:' . implode(',', $customerarray),
            'shopkeeper_id' => 'required|in:' . implode(',', $shopkeeperarray),
            'total' => 'required|numeric',
            'payment_method' => 'required',
        ];
        $messages = [
            'customer_id.required' => 'Nama Customer harus diisi',
            'customer_id.in' => 'Nama Customer tidak ada dalam daftar',
            'shopkeeper_id.required' => 'Nama Shopkeeper harus diisi',
            'shopkeeper_id.in' => 'Nama Shopkeeper tidak ada dalam daftar',
            'total.required' => 'Total Bayar harus diisi',
            'total.numeric' => 'Total Bayar harus berupa angka',
            'payment_method.required' => 'Metode Pembayaran harus diisi',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $bill = new Bill();
        $bill->customer_id = $request->customer_id;
        $bill->shopkeeper_id = $request->shopkeeper_id;
        $bill->total = $request->total;
        $bill->payment_method = $request->payment_method;
        $orders = Order::get()->where('status' , 'PROCESS')->where('customer_id', $request->customer_id);
        if ($orders->isEmpty()) {
        	return redirect()->route('bill.index')->with('message', 'Tidak ada pesanan');
        } else {
            $bill->save();
        	foreach ($orders as $order) {
	        	$order->status = "PAID";
	        	$order->save();
	        }
	        $data = ['orders' => $orders, 'bill' => $bill];
	    	$pdf = PDF::loadView('bill.receipt',$data);
			return $pdf->stream('receipt.pdf');
        }
    }
}

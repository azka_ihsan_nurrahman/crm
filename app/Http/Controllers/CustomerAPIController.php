<?php

namespace App\Http\Controllers;

use App\Customer;
use App\User;
use Illuminate\Http\Request;

class CustomerAPIController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$customer = Customer::find($id);
        $customer = Customer::where('user_id', $id)->first();
        return response()->json($customer, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $request->validate([
            'birth_date' => 'required|date',
            'age' => 'required|numeric',
            'gender' => 'required|string',
            'profession' => 'required|string',
            'marital_status' => 'required|string',
            'address' => 'required|string',
            'city' => 'required|string',
            'phone_number' => 'required|numeric'
        ]);
        $customer->birth_date = $request->birth_date;
        $customer->age = $request->age;
        $customer->gender = $request->gender;
        $customer->profession = $request->profession;
        $customer->marital_status = $request->marital_status;
        $customer->address = $request->address;
        $customer->city = $request->city;
        $customer->phone_number = $request->phone_number;
        $customer->save();
        return response()->json([
            'message' => 'Successfully update customer data'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $user = User::find($customer->user_id);
        $user->delete();        
        $customer->delete();
        return response()->json([
            'message' => 'Customer Data successfully Deleted'
        ]);    
    }
}

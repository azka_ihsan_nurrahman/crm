<?php

namespace App\Http\Controllers;

use Validator;
use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::latest('updated_at')->paginate(10);
        return view('item.index', ['items' => $items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'item_name' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
            'stock' => 'required|numeric',
        ];
        $messages = [
            'item_name.required' => 'Nama Item harus diisi',
            'price.required' => 'Harga Item harus diisi',
            'description.required' => 'Deskripsi Item harus diisi',
            'stock.required' => 'Jumlah Item harus diisi',
            'price.numeric' => 'Harga Item harus berupa angka',
            'stock.numeric' => 'Jumlah Item harus berupa angka',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $item = new Item();
        $item->item_name = $request->item_name;
        $item->price = $request->price;
        $item->description = $request->description;
        $item->stock = $request->stock;
        $item->save();
        return redirect()->route('item.index')->with('message', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return view('item.view', ['item' => $item]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        return view('item.edit', ['item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $rules = [
            'item_name' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
            'stock' => 'required|numeric',
        ];
        $messages = [
            'item_name.required' => 'Nama Item harus diisi',
            'price.required' => 'Harga Item harus diisi',
            'description.required' => 'Deskripsi Item harus diisi',
            'stock.required' => 'Jumlah Item harus diisi',
            'price.numeric' => 'Harga Item harus berupa angka',
            'stock.numeric' => 'Jumlah Item harus berupa angka',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $item->item_name = $request->get('item_name');
        $item->price = $request->get('price');
        $item->description = $request->get('description');
        $item->stock = $request->get('stock');
        $item->save();
        return redirect()->route('item.index')->with('message', 'Data berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $item->delete();
        return redirect()->route('item.index')->with('message', 'Data berhasil dihapus');
    }
}

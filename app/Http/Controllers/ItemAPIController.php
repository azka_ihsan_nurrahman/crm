<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemAPIController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Item::find($id);
        return response()->json($item, 200)->header('Content-Type', 'application/json');
    }
}

<?php

namespace App\Http\Controllers;

use Validator;
use App\Customer;
use App\Item;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        if (isset($request->customer_id)) {
            $orders = Order::get()->where('status', 'PROCESS')->where('customer_id', $request->customer_id);
        } else {
            $orders = Order::get()->where('status' , 'PROCESS');
        }
        $customers = Customer::get();
        return view('order.index', ['orders' => $orders, 'customers' => $customers]);
    }
    public function create() 
    {
    	$items = Item::get();
    	$customers = Customer::get();
    	return view('order.create', ['items' => $items, 'customers' => $customers]);
    }
    public function store(Request $request)
    {
    	$items = Item::get();
    	$itemarray = array();
    	foreach ($items as $item) {
    		array_push($itemarray, $item->item_name);
    	}
    	$customers = Customer::get();
    	$customerarray = array();
    	foreach ($customers as $customer) {
    		array_push($customerarray, $customer->customer_name);
    	}
        $rules = [
        	'customer_name' => 'required|in:' . implode(',', $customerarray),
            'item_name' => 'required|in:' . implode(',', $itemarray),
            'quantity' => 'required|numeric',
        ];
        $messages = [
            'item_name.required' => 'Nama Barang harus diisi',
            'item_name.in' => 'Nama Barang tidak ada dalam daftar',
            'customer_name.required' => 'Nama Customer harus diisi',
            'customer_name.in' => 'Nama Customer tidak ada dalam daftar',
            'quantity.required' => 'Jumlah Barang harus diisi',
            'quantity.numeric' => 'Jumlah Barang harus berupa angka',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $order = new Order();
        $item = Item::where('item_name', $request->item_name)->first();
        $customer = Customer::where('customer_name', $request->customer_name)->first();       
        $order->customer_id = $customer->id;
        $order->item_id = $item->id;
        $order->loyalty_program_id = $customer->loyalty_program_id;
        $order->quantity = $request->quantity;
        $order->subtotal = ($item->price * $order->quantity) - ($item->price * $order->quantity * ($customer->loyaltyprogram->discount / 100));
        $order->status = "PROCESS";
        $item->stock = $item->stock - $request->quantity;
        if ($item->stock < 0) {
            return redirect()->route('order.index')->with('message', 'Stok Barang Telah Habis/Kurang Cukup');
        } else {
            $order->save();
            $item->save();
            return redirect()->route('order.index')->with('message', 'Data berhasil ditambahkan');
        }
    }
    public function show(Order $order)
    {

    }
    public function edit(Order $order)
    {
    	$items = Item::get();
    	$itemorder = Item::where('id', $order->item_id)->first();
		return view('order.edit', ['order' => $order, 'items' => $items, 'itemorder' => $itemorder]);
    }
    public function update(Request $request, Order $order)
    {
    	$items = Item::get();
    	$itemarray = array();
    	foreach ($items as $item) {
    		array_push($itemarray, $item->item_name);
    	}
    	//dd($itemarray);
        $rules = [
            'item_name' => 'required|in:' . implode(',', $itemarray),
            'quantity' => 'required|numeric',
        ];
        $messages = [
            'item_name.required' => 'Nama Barang harus diisi',
            'item_name.in' => 'Nama Barang tidak ada dalam daftar',
            'quantity.required' => 'Jumlah Barang harus diisi',
            'quantity.numeric' => 'Jumlah Barang harus berupa angka',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $item = Item::where('item_name', $request->item_name)->first();
        $item->stock = $item->stock + $order->quantity;     
        $order->item_id = $item->id;
        $order->quantity = $request->quantity;
        $order->subtotal = ($item->price * $order->quantity) - ($item->price * $order->quantity * ($order->loyaltyprogram->discount / 100));
        $item->stock = $item->stock - $request->quantity;
        if ($item->stock < 0) {
            return redirect()->route('order.index')->with('message', 'Stok Barang Telah Habis/Kurang Cukup');
        } else {
            $order->save();
            $item->save();
            return redirect()->route('order.index')->with('message', 'Data berhasil diedit');
        }
    }
    public function destroy(Order $order)
    {
        $order->delete();
        return redirect()->route('order.index')->with('message', 'Data berhasil dihapus');
    }
}

<?php

namespace App\Http\Controllers;

use Validator;
use App\Customer;
use App\Item;
use App\Order;
use Illuminate\Http\Request;

class OrderAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $orders = Order::with('items')->where('status', 'PROCESS')->where('customer_id', $id)->get();
        return response()->json($orders, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'customer_id' => 'required|numeric',
            'item_id' => 'required|numeric',
            'quantity' => 'required|numeric'
        ];
        $messages = [
            'item_id.required' => 'Nama Barang harus diisi',
            'item_id.numeric' => 'Nama Barang harus berupa angka',
            'customer_id.required' => 'Nama Customer harus diisi',
            'customer_id.numeric' => 'Nama Customer harus berupa angka',
            'quantity.required' => 'Jumlah Barang harus diisi',
            'quantity.numeric' => 'Jumlah Barang harus berupa angka',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $order = new Order();
        $customer = Customer::find($request->customer_id);
        $item = Item::find($request->item_id);       
        $order->customer_id = $request->customer_id;
        $order->item_id = $request->item_id;
        $order->loyalty_program_id = $customer->loyalty_program_id;

        $order->quantity = $request->quantity;
        $order->subtotal = ($item->price * $order->quantity) - ($item->price * $order->quantity * ($customer->loyaltyprogram->discount / 100));
        $order->status = "PROCESS";
        $item->stock = $item->stock - $request->quantity;

        if ($item->stock < 0) {
            return response()->json([
                'message' => 'Item out of stock'
            ]);
        } else {
            $order->save();
            $item->save();
            return response()->json([
                'message' => 'Successfully order item'
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'quantity' => 'required|numeric'
        ];
        $messages = [
            'quantity.required' => 'Jumlah Barang harus diisi',
            'quantity.numeric' => 'Jumlah Barang harus berupa angka'
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $order = Order::find($id);

        $item = Item::find($order->item_id);
        $item->stock = $item->stock + $order->quantity;

        $order->quantity = $request->quantity;
        $order->subtotal = ($item->price * $order->quantity) - ($item->price * $order->quantity * ($order->loyaltyprogram->discount / 100));
        $order->status = "PROCESS";
        $item->stock = $item->stock - $request->quantity;

        if ($item->stock < 0) {
            return response()->json([
                'message' => 'Item insufficient'
            ]);
        } else {
            $order->save();
            $item->save();
            return response()->json([
                'message' => 'Successfully update order item quantity'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();
        return response()->json([
            'message' => 'Order Data successfully Deleted'
        ]);  
    }
}

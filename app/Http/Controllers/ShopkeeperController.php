<?php

namespace App\Http\Controllers;

use Validator;
use App\Shopkeeper;
use Illuminate\Http\Request;

class ShopkeeperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shopkeepers = Shopkeeper::latest('updated_at')->paginate(10);
        return view('shopkeeper.index', ['shopkeepers' => $shopkeepers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shopkeeper.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'shopkeeper_name' => 'required',
            'owner_name' => 'required',
            'owner_ktp' => 'required',
            'address' => 'required',
            'city' => 'required',
        ];
        $messages = [
            'shopkeeper_name.required' => 'Nama Shopkeeper harus diisi',
            'owner_name.required' => 'Nama Owner harus diisi',
            'owner_ktp.required' => 'KTP Owner harus diisi',
            'address.required' => 'Alamat Shopkeeper harus diisi',
            'city.required' => 'Kota Shopkeeper harus diisi',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $shopkeeper = new Shopkeeper();
        $shopkeeper->shopkeeper_name = $request->shopkeeper_name;
        $shopkeeper->owner_name = $request->owner_name;
        $shopkeeper->owner_ktp = $request->owner_ktp;
        $shopkeeper->address = $request->address;
        $shopkeeper->city = $request->city;
        $shopkeeper->status = "Aktif";
        $shopkeeper->save();
        return redirect()->route('shopkeeper.index')->with('message', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Shopkeeper $shopkeeper)
    {
        return view('shopkeeper.view', ['shopkeeper' => $shopkeeper]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Shopkeeper $shopkeeper)
    {
        return view('shopkeeper.edit', ['shopkeeper' => $shopkeeper]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shopkeeper $shopkeeper)
    {
        $rules = [
            'shopkeeper_name' => 'required',
            'owner_name' => 'required',
            'owner_ktp' => 'required',
            'address' => 'required',
            'city' => 'required',
        ];
        $messages = [
            'shopkeeper_name.required' => 'Nama Shopkeeper harus diisi',
            'owner_name.required' => 'Nama Owner harus diisi',
            'owner_ktp.required' => 'KTP Owner harus diisi',
            'address.required' => 'Alamat Shopkeeper harus diisi',
            'city.required' => 'Kota Shopkeeper harus diisi',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $shopkeeper->shopkeeper_name = $request->get('shopkeeper_name');
        $shopkeeper->owner_name = $request->get('owner_name');
        $shopkeeper->owner_ktp = $request->get('owner_ktp');
        $shopkeeper->address = $request->get('address');
        $shopkeeper->city = $request->get('city');
        $shopkeeper->save();
        return redirect()->route('shopkeeper.index')->with('message', 'Data berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shopkeeper $shopkeeper)
    {
        $shopkeeper->delete();
        return redirect()->route('shopkeeper.index')->with('message', 'Data berhasil dihapus');
    }
}

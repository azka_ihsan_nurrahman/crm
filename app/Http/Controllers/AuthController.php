<?php

namespace App\Http\Controllers;

use App\Customer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Mail\Mailable;
use App\Mail\ForgetPassword; 
use Carbon\Carbon;
use Laravel\Passport\HasApiTokens;

class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] role_id
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'loyalty_program_id' => 'required|numeric',
            'birth_date' => 'required|date',
            'age' => 'required|numeric',
            'gender' => 'required|string',
            'profession' => 'required|string',
            'marital_status' => 'required|string',
            'address' => 'required|string',
            'city' => 'required|string',
            'phone_number' => 'required|numeric'
        ]);

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'account_level' => 1
        ]);

        $user->save();

        $userdata = User::where('email', $request->email)->first();
        $customer = new Customer();
        $customer->user_id = $userdata->id;
        $customer->loyalty_program_id = $request->loyalty_program_id;
        $customer->customer_name = $request->name;
        $customer->email = $request->email;
        $customer->birth_date = $request->birth_date;
        $customer->age = $request->age;
        $customer->gender = $request->gender;
        $customer->profession = $request->profession;
        $customer->marital_status = $request->marital_status;
        $customer->address = $request->address;
        $customer->city = $request->city;
        $customer->phone_number = $request->phone_number;
        $customer->save();

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    /**
     * Login user
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $tokenResult = $this->renew($request);

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    /**
     * Get and create new token 
     *
     * @return token object
     */
    public function renew(Request $request)
    {
        $user = $request->user();

        $tokenResult = $user->createToken('CRM Token');
        $token = $tokenResult->token;

        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();
        return $tokenResult;
    }
    /**
     * Forget Password
     *
     * @param [string] email
     * @return email to user
     */
    public function forget(Request $request) 
    {
        $user = User::where('email', $request->email)->first();
        if (is_null($user)) {
            return response()->json([
                'message' => 'Email tidak terdaftar'
            ]);
        }
        $encrypted_id = encrypt(($user->id ." ". $user->email ." ".date('Y-m-d')));
        $link = "http://localhost:8000/api/auth/reset/" . $encrypted_id;
        Mail::to($request->email)->send(new ForgetPassword($link));
        return response()->json([
            'message' => 'Reset Password Mail has been sent'
        ]);
    }
    /**
     * Reset Password
     *
     * @param [string] id
     * @return [string] message
     */
    public function reset(Request $request, $id) 
    {
        $decrypted_id = decrypt($id);
        $split = explode(' ', $decrypted_id, 3);
        if (strtotime(date('Y-m-d')) > strtotime($split[2] . ' +1 day')) {
            return response()->json([
                'message' => 'Reset Password Link has been Expired'
            ]);            
        }
        $user = User::find($split[0]);
        $user->name = $user->name;
        $user->email = $user->email;
        $user->password = bcrypt("123456");
        $user->save();
        return response()->json([
            'message' => 'Password for user with email ' .$split[1]. ' has been reset to 123456'
        ]);
    }    
}

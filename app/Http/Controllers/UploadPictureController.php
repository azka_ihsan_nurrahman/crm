<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class UploadPictureController extends Controller
{
	public function upload($id) {
		$item = Item::find($id);
		return view('item.upload', ['item' => $item]);
	}
    public function store(Request $request, $id) {
    	$item = Item::find($id);
		$this->validate($request, [
	      'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
	    ]);
	    if ($request->hasFile('image')) {
        	$imageName = $item->item_name.'.'.$request->image->getClientOriginalExtension();
        	$request->image->move(public_path('img\item'), $imageName);
        	$item->picture_url = 'img/item/'. $imageName;
	    }
	    $item->save();
	    return redirect()->route('item.index')->with('message', 'Gambar Berhasil Ditambahkan.');
    }
}

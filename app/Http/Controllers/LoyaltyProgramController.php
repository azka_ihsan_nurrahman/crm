<?php

namespace App\Http\Controllers;

use App\LoyaltyProgram;
use App\Http\Requests\LoyaltyProgramStore;
use Illuminate\Http\Request;

class LoyaltyProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loyaltyprograms = LoyaltyProgram::latest('updated_at')->paginate(10);
        return view('loyaltyprogram.index', ['loyaltyprograms' => $loyaltyprograms]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('loyaltyprogram.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LoyaltyProgramStore $request)
    {
        $loyaltyprogram = new LoyaltyProgram();
        $loyaltyprogram->fill($request->all());
        $loyaltyprogram->save();
        return redirect()->route('loyaltyprogram.index')->with('message', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LoyaltyProgram $loyaltyprogram)
    {
        return view('loyaltyprogram.view', ['loyaltyprogram' => $loyaltyprogram]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LoyaltyProgram $loyaltyprogram)
    {
        return view('loyaltyprogram.edit', ['loyaltyprogram' => $loyaltyprogram]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LoyaltyProgramStore $request, LoyaltyProgram $loyaltyprogram)
    {
        $loyaltyprogram->fill($request->all());
        $loyaltyprogram->save();
        return redirect()->route('loyaltyprogram.index')->with('message', 'Data berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoyaltyProgram $loyaltyprogram)
    {
        $loyaltyprogram->delete();
        return redirect()->route('loyaltyprogram.index')->with('message', 'Data berhasil dihapus');
    }
}

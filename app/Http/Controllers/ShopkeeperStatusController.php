<?php

namespace App\Http\Controllers;

use Validator;
use App\Shopkeeper;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ShopkeeperStatusController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'status.required' => 'Status harus diisi',
        ];
        Validator::make($request->all(), [
            'status' => [
                'required',
                Rule::in(['Aktif', 'Nonaktif']),
            ]], $messages)->validate();
        $shopkeeper = Shopkeeper::find($id);
        $shopkeeper->status = $request->status;
        $shopkeeper->save();
        return redirect()->route('shopkeeper.index')->with('message', 'Status berhasil diubah');
    }
}

<?php

namespace App\Http\Controllers;

use PDF;
use App\Customer;
use Illuminate\Http\Request;

class CustomerProfileController extends Controller
{
    public function index()
    {
    	$malecustomers = Customer::where('gender', 'Pria')->count();
    	$femalecustomers = Customer::where('gender', 'Wanita')->count();
    	$marriedcustomers = Customer::where('marital_status', 'Sudah Menikah')->count();
    	$singlecustomers = Customer::where('marital_status', 'Belum Menikah')->count();
    	$childcustomers = Customer::whereBetween('age', [0, 13])->count();
    	$teencustomers = Customer::whereBetween('age', [14, 21])->count();
    	$productivecustomers = Customer::whereBetween('age', [22, 40])->count();
    	$adultcustomers = Customer::whereBetween('age', [41, 60])->count();
    	$eldercustomers = Customer::whereBetween('age', [61, 1000])->count();
    	$customers = Customer::get();
    	$customercity = array();
    	$customerprofession = array();
    	$indexcity = 0;
    	$indexprofession = 0;
		foreach ($customers as $customer) {
			if (in_array($customer->city, array_column($customercity, 'city'))) {
				$id = array_search($customer->city, array_column($customercity, 'city'));
				$customercity[$id]['count'] = $customercity[$id]['count'] + 1;
			} else {
				$customercity[$indexcity]['city'] = $customer->city;
				$customercity[$indexcity]['count'] = 1;
				$indexcity++;
			}
			if (in_array($customer->profession, array_column($customerprofession, 'profession'))) {
				$id = array_search($customer->profession, array_column($customerprofession, 'profession'));
				$customerprofession[$id]['count'] = $customerprofession[$id]['count'] + 1;
			} else {
				$customerprofession[$indexprofession]['profession'] = $customer->profession;
				$customerprofession[$indexprofession]['count'] = 1;
				$indexprofession++;
			}
		}
    	return view('customerprofile.index', ['customercity' => $customercity, 'customerprofession' => $customerprofession, 'malecustomers' => $malecustomers, 'femalecustomers' => $femalecustomers, 'marriedcustomers' => $marriedcustomers, 'singlecustomers' => $singlecustomers, 'childcustomers' => $childcustomers, 'teencustomers' => $teencustomers, 'productivecustomers' => $productivecustomers, 'adultcustomers' => $adultcustomers, 'eldercustomers' => $eldercustomers]);
    }
    public function reportcitysegment(Request $request)
    {
    	$customers = Customer::get();
    	$customercity = array();
    	$indexcity = 0;
		foreach ($customers as $customer) {
			if (in_array($customer->city, array_column($customercity, 'city'))) {
				$id = array_search($customer->city, array_column($customercity, 'city'));
				$customercity[$id]['count'] = $customercity[$id]['count'] + 1;
			} else {
				$customercity[$indexcity]['city'] = $customer->city;
				$customercity[$indexcity]['count'] = 1;
				$indexcity++;
			}
		}
		$filename = "customercitysegmentreport.pdf";
		$data = ['customercity' => $customercity];
    	$pdf = PDF::loadView('customerprofile.citysegmentreport', $data);
		return $pdf->stream($filename);
    }
    public function reportprofessionsegment(Request $request)
    {
    	$customers = Customer::get();
    	$customerprofession = array();
    	$indexprofession = 0;
		foreach ($customers as $customer) {
			if (in_array($customer->profession, array_column($customerprofession, 'profession'))) {
				$id = array_search($customer->profession, array_column($customerprofession, 'profession'));
				$customerprofession[$id]['count'] = $customerprofession[$id]['count'] + 1;
			} else {
				$customerprofession[$indexprofession]['profession'] = $customer->profession;
				$customerprofession[$indexprofession]['count'] = 1;
				$indexprofession++;
			}
		}
		$filename = "customerprofessionsegmentreport.pdf";
		$data = ['customerprofession' => $customerprofession];
    	$pdf = PDF::loadView('customerprofile.professionsegmentreport', $data);
		return $pdf->stream($filename);    	
    }
}

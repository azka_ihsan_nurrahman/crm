<?php

namespace App\Http\Controllers;

use PDF;
use App\Bill;
use App\Services\FinanceReportService;
use Illuminate\Http\Request;

class FinanceReportController extends Controller
{
	private $service;
    
    public function __construct(FinanceReportService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
    	$datacash = $this->service->showcash();
    	$datadebit = $this->service->showdebit();
    	$datacredit = $this->service->showcredit();
    	return view('financereport.index', ['datacash' => $datacash, 'datadebit' => $datadebit, 'datacredit' => $datacredit]);
    }
    public function printreport(Request $request)
    {
    	$datacash = $this->service->showcash();
    	$datadebit = $this->service->showdebit();
    	$datacredit = $this->service->showcredit();
    	$data = ['datacash' => $datacash, 'datadebit' => $datadebit, 'datacredit' => $datacredit];
	    $pdf = PDF::loadView('financereport.report',$data);
		return $pdf->stream('financereport.pdf');
    }
}

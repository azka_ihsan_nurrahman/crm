<?php

namespace App\Http\Controllers;

use PDF;
use App\Customer;
use Illuminate\Http\Request;

class CustomerProfileDetailController extends Controller
{
    public function showgendersegment(Request $request, $gender)
    {
    	$customers = Customer::where('gender', $gender)->paginate(10);
    	return view('customerprofile.viewgendersegment', ['customers' => $customers, 'gender' => $gender]);
    }
    public function showagesegment(Request $request, $age)
    {
    	if ($age == "anak") {
    		$batasbawah = 0;
    		$batasatas = 13;
    	} else if ($age == "remaja") {
    		$batasbawah = 14;
    		$batasatas = 21;
    	} else if ($age == "produktif") {
    		$batasbawah = 22;
    		$batasatas = 40;
    	} else if ($age == "dewasa") {
    		$batasbawah = 41;
    		$batasatas = 60;
    	} else if ($age == "lansia") {
    		$batasbawah = 60;
    		$batasatas = 1000;
    	}
    	$customers = Customer::whereBetween('age', [$batasbawah, $batasatas])->paginate(10);
    	return view('customerprofile.viewagesegment', ['customers' => $customers, 'age' => $age]);    	
    }
    public function showmaritalsegment(Request $request, $marital)
    {
    	if ($marital == "sudah") {
    		$status = "Sudah Menikah";
    	} else if ($marital == "belum") {
    		$status = "Belum Menikah";
    	}
    	$customers = Customer::where('marital_status', $status)->paginate(10);
    	return view('customerprofile.viewmaritalsegment', ['customers' => $customers, 'marital' => $status]);    	
    }
    public function reportgendersegment(Request $request, $gender)
    {
    	$customers = Customer::where('gender', $gender)->get();
		$filename = "customergendersegmentreport.pdf";
		$data = ['customers' => $customers, 'gender' => $gender];
    	$pdf = PDF::loadView('customerprofile.gendersegmentreport', $data);
		return $pdf->stream($filename);
    }
    public function reportagesegment(Request $request, $age)
    {
    	if ($age == "anak") {
    		$batasbawah = 0;
    		$batasatas = 13;
    	} else if ($age == "remaja") {
    		$batasbawah = 14;
    		$batasatas = 21;
    	} else if ($age == "produktif") {
    		$batasbawah = 22;
    		$batasatas = 40;
    	} else if ($age == "dewasa") {
    		$batasbawah = 41;
    		$batasatas = 60;
    	} else if ($age == "lansia") {
    		$batasbawah = 60;
    		$batasatas = 1000;
    	}
    	$customers = Customer::whereBetween('age', [$batasbawah, $batasatas])->get();
		$filename = "customeragesegmentreport.pdf";
		$data = ['customers' => $customers, 'age' => $age];
    	$pdf = PDF::loadView('customerprofile.agesegmentreport', $data);
		return $pdf->stream($filename);    	
    }
    public function reportmaritalsegment(Request $request, $marital)
    {
    	$customers = Customer::where('marital_status', $marital)->get();
		$filename = "customermaritalsegmentreport.pdf";
		$data = ['customers' => $customers, 'marital' => $marital];
    	$pdf = PDF::loadView('customerprofile.maritalsegmentreport', $data);
		return $pdf->stream($filename);    	
    }
}

<?php

namespace App\Http\Controllers;

use App\Events\LoginEvent;
use App\Services\DashboardService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    private $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(DashboardService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        //event(new LoginEvent($user));
        if ($user->account_level == 4) {
            return view('homeadmin');
        } else if ($user->account_level == 3) {
            $datafinance = $this->service->showoverallfinance();
            $datatrend = $this->service->showoveralltrend();
            $customerprofiledata = $this->service->showoverallcustomer();
            return view('homemanager', ['datafinance' => $datafinance, 'datatrend' => $datatrend, 'customerprofiledata' => $customerprofiledata]);
        } else if ($user->account_level == 2) {
            return view('homestaff');
        } else if ($user->account_level == 1) {
            return view('homecustomer');
        }
        $request->session()->put('user_id',$user->id);
    }
}

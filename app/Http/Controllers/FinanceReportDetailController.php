<?php

namespace App\Http\Controllers;

use PDF;
use App\Bill;
use Illuminate\Http\Request;

class FinanceReportDetailController extends Controller
{
public function cashindex() 
    {
    	$billscash = Bill::where('payment_method' , 'Cash')->paginate(10);
    	return view('financereport.cash', ['billscash' => $billscash]);
    }
    public function debitindex() 
    {
    	$billsdebit = Bill::where('payment_method' , 'Debit')->paginate(10);
    	return view('financereport.debit', ['billsdebit' => $billsdebit]);
    }
    public function creditindex() 
    {
    	$billscredit = Bill::where('payment_method' , 'Credit')->paginate(10);
    	return view('financereport.credit', ['billscredit' => $billscredit]);
    }
    public function printcashreport(Request $request)
    {
    	$billscash = Bill::where('payment_method' , 'Cash')->get();
    	$data = ['billscash' => $billscash];
	    $pdf = PDF::loadView('financereport.cashreport',$data);
		return $pdf->stream('cashreport.pdf');
    }
    public function printdebitreport(Request $request)
    {
    	$billsdebit = Bill::where('payment_method' , 'Debit')->get();
    	$data = ['billsdebit' => $billsdebit];
	    $pdf = PDF::loadView('financereport.debitreport',$data);
		return $pdf->stream('debitreport.pdf');
    }
    public function printcreditreport(Request $request)
    {
    	$billscredit = Bill::where('payment_method' , 'Credit')->get();
    	$data = ['billscredit' => $billscredit];
	    $pdf = PDF::loadView('financereport.creditreport',$data);
		return $pdf->stream('creditreport.pdf');
    }
}

<?php

namespace App\Http\Controllers;

use App\LoyaltyProgram;
use Illuminate\Http\Request;

class LoyaltyProgramAPIController extends Controller
{
	/**
     * Get All Loyalty Program List
     *
     * @return [json] Loyalty Program List
     */
    public function index(Request $request)
    {
    	$loyaltyprograms = LoyaltyProgram::all();
    	return response()->json($loyaltyprograms, 200)->header('Content-Type', 'application/json');
    }
    public function show($id)
    {
        $loyaltyprogram = LoyaltyProgram::find($id);
        return response()->json($loyaltyprogram, 200)->header('Content-Type', 'application/json');
    }
}

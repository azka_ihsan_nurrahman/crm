<?php

namespace App\Http\Controllers;

use PDF;
use App\Bill;
use Illuminate\Http\Request;

class OmzetReportController extends Controller
{
    public function index()
    {
    	$bills = Bill::get();
    	$totalomzet = 0;
    	foreach ($bills as $bill) {
    		$totalomzet = $totalomzet + $bill->total;
    	}
    	$billsyearly = Bill::whereBetween('created_at', [date("Y-m-d h:i:sa", strtotime("-1 year")), date("Y-m-d h:i:sa")])->get();
    	$omzetyear = 0;
    	foreach ($billsyearly as $billyearly) {
    		$omzetyear = $omzetyear + $billyearly->total;
    	}
    	$billsquarterly = Bill::whereBetween('created_at', [date("Y-m-d h:i:sa", strtotime("-3 month")), date("Y-m-d h:i:sa")])->get();
    	$omzetquarter = 0;
    	foreach ($billsquarterly as $billquarterly) {
    		$omzetquarter = $omzetquarter + $billquarterly->total;
    	}
    	$billsmonthly = Bill::whereBetween('created_at', [date("Y-m-d h:i:sa", strtotime("-1 month")), date("Y-m-d h:i:sa")])->get();
    	$omzetmonth = 0;
    	foreach ($billsmonthly as $billmonthly) {
    		$omzetmonth = $omzetmonth + $billmonthly->total;
    	}
    	$billsweekly = Bill::whereBetween('created_at', [date("Y-m-d h:i:sa", strtotime("-1 week")), date("Y-m-d h:i:sa")])->get();
    	$omzetweek = 0;
    	foreach ($billsweekly as $billweekly) {
    		$omzetweek = $omzetweek + $billweekly->total;
    	}
    	$billsdaily = Bill::whereBetween('created_at', [date("Y-m-d h:i:sa", strtotime("yesterday")), date("Y-m-d h:i:sa")])->get();
    	$omzetday = 0;
    	foreach ($billsdaily as $billdaily) {
    		$omzetday = $omzetday + $billdaily->total;
    	}
    	return view('omzetreport.index', ["omzetyear" => $omzetyear, "omzetquarter" => $omzetquarter, "omzetmonth" => $omzetmonth, "omzetweek" => $omzetweek, "omzetday" => $omzetday, "totalomzet" => $totalomzet]);
    }
    public function show($time)
    {
    	$bills = Bill::whereBetween('created_at', [date("Y-m-d h:i:sa", strtotime($time)), date("Y-m-d h:i:sa")])->paginate(10); 
    	return view('omzetreport.view', ["bills" => $bills, "time" => $time]);   	
    }
    public function printomzetreport(Request $request)
    {
    	$bills = Bill::get();
    	$totalomzet = 0;
    	foreach ($bills as $bill) {
    		$totalomzet = $totalomzet + $bill->total;
    	}
    	$billsyearly = Bill::whereBetween('created_at', [date("Y-m-d h:i:sa", strtotime("-1 year")), date("Y-m-d h:i:sa")])->get();
    	$omzetyear = 0;
    	foreach ($billsyearly as $billyearly) {
    		$omzetyear = $omzetyear + $billyearly->total;
    	}
    	$billsquarterly = Bill::whereBetween('created_at', [date("Y-m-d h:i:sa", strtotime("-3 month")), date("Y-m-d h:i:sa")])->get();
    	$omzetquarter = 0;
    	foreach ($billsquarterly as $billquarterly) {
    		$omzetquarter = $omzetquarter + $billquarterly->total;
    	}
    	$billsmonthly = Bill::whereBetween('created_at', [date("Y-m-d h:i:sa", strtotime("-1 month")), date("Y-m-d h:i:sa")])->get();
    	$omzetmonth = 0;
    	foreach ($billsmonthly as $billmonthly) {
    		$omzetmonth = $omzetmonth + $billmonthly->total;
    	}
    	$billsweekly = Bill::whereBetween('created_at', [date("Y-m-d h:i:sa", strtotime("-1 week")), date("Y-m-d h:i:sa")])->get();
    	$omzetweek = 0;
    	foreach ($billsweekly as $billweekly) {
    		$omzetweek = $omzetweek + $billweekly->total;
    	}
    	$billsdaily = Bill::whereBetween('created_at', [date("Y-m-d h:i:sa", strtotime("yesterday")), date("Y-m-d h:i:sa")])->get();
    	$omzetday = 0;
    	foreach ($billsdaily as $billdaily) {
    		$omzetday = $omzetday + $billdaily->total;
    	}
    	$data = ["omzetyear" => $omzetyear, "omzetquarter" => $omzetquarter, "omzetmonth" => $omzetmonth, "omzetweek" => $omzetweek, "omzetday" => $omzetday, "totalomzet" => $totalomzet];
	    $pdf = PDF::loadView('omzetreport.report', $data);
		return $pdf->stream('omzetreport.pdf');
    }
    public function printreportdetail(Request $request, $time)
    {
    	$bills = Bill::whereBetween('created_at', [date("Y-m-d h:i:sa", strtotime($time)), date("Y-m-d h:i:sa")])->get(); 
		$data = ["bills" => $bills, "time" => $time];
    	$pdf = PDF::loadView('omzetreport.detailreport',$data);
		return $pdf->stream('omzetdetailreport.pdf');
    }
}

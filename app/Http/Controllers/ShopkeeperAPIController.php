<?php

namespace App\Http\Controllers;

use App\Shopkeeper;
use Illuminate\Http\Request;

class ShopkeeperAPIController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shopkeeper = Shopkeeper::get();
        return response()->json($shopkeeper, 200)->header('Content-Type', 'application/json');
    }
}

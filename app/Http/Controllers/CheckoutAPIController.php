<?php

namespace App\Http\Controllers;

use PDF;
use Validator;
use App\Bill;
use App\Customer;
use App\Order;
use App\Shopkeeper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailable;
use App\Mail\PaymentReceipt; 
use Illuminate\Validation\Rule;

class CheckoutAPIController extends Controller
{
    public function checkout(Request $request)
    {
        $rules = [
        	'customer_id' => 'required',
            'shopkeeper_id' => 'required',
            'payment_method' => 'required',
        ];
        $messages = [
            'customer_id.required' => 'Nama Customer harus diisi',
            'shopkeeper_id.required' => 'Nama Shopkeeper harus diisi',
            'payment_method.required' => 'Metode Pembayaran harus diisi',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $bill = new Bill();
        $bill->customer_id = $request->customer_id;
        $bill->shopkeeper_id = $request->shopkeeper_id;
        $bill->total = 0;
        $bill->payment_method = $request->payment_method;
        $customer = Customer::find($request->customer_id);
        $orders = Order::get()->where('status' , 'PROCESS')->where('customer_id', $request->customer_id);
        if ($orders->isEmpty()) {
            return response()->json([
                'message' => 'There is no order'
            ]);
        } else {
        	foreach ($orders as $order) {
        		$bill->total = $bill->total + $order->subtotal;
	        	$order->status = "PAID";
	        	$order->save();
	        }
	        $bill->save();
	        $data = ['orders' => $orders, 'bill' => $bill];
	        Mail::to($customer->email)->send(new PaymentReceipt($bill, $orders));
	    	$pdf = PDF::loadView('bill.receipt',$data);
			return $pdf->stream('receipt.pdf');
        }
    }
}

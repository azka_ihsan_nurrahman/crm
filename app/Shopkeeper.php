<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shopkeeper extends Model
{
    public function bills()
    {
        return $this->hasMany(Bill::class, 'shopkeeper_id');
    }
}

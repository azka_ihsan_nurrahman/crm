<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    public function customer() 
    {
    	return $this->belongsTo(Customer::class, 'customer_id');
    }
    public function shopkeeper() 
    {
    	return $this->belongsTo(Shopkeeper::class, 'shopkeeper_id');
    }
}

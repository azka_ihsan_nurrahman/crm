<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentReceipt extends Mailable
{
    use Queueable, SerializesModels;

    public $bill;
    public $orders;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bill, $orders)
    {
        $this->bill = $bill;
        $this->orders = $orders;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'cashier@crm.io';
        $name = 'CRM Plaform';
        $subject = 'Payment Receipt';

        return $this->view('bill.receipt')
                    ->from($address, $name)
                    ->cc($address, $name)
                    ->bcc($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject);
    }
}

-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 20 Jan 2021 pada 04.12
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `account_levels`
--

CREATE TABLE `account_levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `level_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `account_levels`
--

INSERT INTO `account_levels` (`id`, `level_description`) VALUES
(1, 'Customer'),
(2, 'Staff'),
(3, 'Manager'),
(4, 'Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `authentication_log`
--

CREATE TABLE `authentication_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `authenticatable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `authenticatable_id` bigint(20) UNSIGNED NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_at` timestamp NULL DEFAULT NULL,
  `logout_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `authentication_log`
--

INSERT INTO `authentication_log` (`id`, `authenticatable_type`, `authenticatable_id`, `ip_address`, `user_agent`, `login_at`, `logout_at`) VALUES
(1, 'App\\User', 2, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0', '2021-01-19 20:07:06', '2021-01-19 20:07:54'),
(2, 'App\\User', 4, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0', '2021-01-19 20:08:03', '2021-01-19 20:09:22'),
(3, 'App\\User', 4, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0', '2021-01-19 20:09:29', '2021-01-19 20:09:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bills`
--

CREATE TABLE `bills` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `shopkeeper_id` int(10) UNSIGNED NOT NULL,
  `total` int(11) NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `loyalty_program_id` int(10) UNSIGNED NOT NULL,
  `QR_Code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profession` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marital_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `customers`
--

INSERT INTO `customers` (`id`, `user_id`, `loyalty_program_id`, `QR_Code`, `customer_name`, `birth_date`, `age`, `gender`, `profession`, `marital_status`, `email`, `address`, `city`, `phone_number`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'img/250x250.png', 'Customer 1', '1993-04-11', 25, 'Pria', 'Programmer', 'Belum Menikah', 'customer1@gmail.com', 'Jl. Pungkur No. 5', 'Surakarta', '082116140638', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `items`
--

INSERT INTO `items` (`id`, `item_name`, `price`, `description`, `picture_url`, `stock`, `created_at`, `updated_at`) VALUES
(1, 'Kaos Oblong', 50000, 'Kaos Dagadu Oblong Khas Jogja', 'img/250x250.png', 50, NULL, NULL),
(2, 'Baju Berkerah', 100000, 'Baju Kerah Asli Madiun', 'img/250x250.png', 50, NULL, NULL),
(3, 'Celana L', 57000, 'Celana Khas Bali', 'img/250x250.png', 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `loyalty_programs`
--

CREATE TABLE `loyalty_programs` (
  `id` int(10) UNSIGNED NOT NULL,
  `program_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `discount` double(8,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `loyalty_programs`
--

INSERT INTO `loyalty_programs` (`id`, `program_name`, `start_date`, `end_date`, `discount`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Weekend Sale', '2018-04-11', '2018-04-15', 50.00, 'Diskon Akhir Minggu', NULL, NULL),
(2, 'Weekday Sale', '2018-04-16', '2018-04-20', 10.00, 'Diskon Akhir Minggu', NULL, NULL),
(3, 'Member Discount', '2018-04-01', '2018-04-30', 15.00, 'Diskon Akhir Minggu', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2017_09_01_000000_create_authentication_log_table', 1),
(9, '2018_04_11_035453_create_loyalty_programs_table', 1),
(10, '2018_04_11_040600_create_account_levels_table', 1),
(11, '2018_04_11_040745_create_items_table', 1),
(12, '2018_04_11_041156_create_shopkeepers_table', 1),
(13, '2018_04_11_041508_create_customers_table', 1),
(14, '2018_04_11_042355_create_orders_table', 1),
(15, '2018_04_11_043008_create_bills_table', 1),
(16, '2018_05_04_110418_create_sessions_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'CRM Personal Access Client', 'j0J05kuW2YPD25giAtKXSv6OwOLTKF5UfOcZps3q', 'http://localhost', 1, 0, 0, '2021-01-19 20:05:13', '2021-01-19 20:05:13'),
(2, NULL, 'CRM Password Grant Client', 'oboRyYVm9Rnzo9ClzQBXXwOWndwWh4Fmq0J1ZDZL', 'http://localhost', 0, 1, 0, '2021-01-19 20:05:13', '2021-01-19 20:05:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-01-19 20:05:13', '2021-01-19 20:05:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `loyalty_program_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `shopkeepers`
--

CREATE TABLE `shopkeepers` (
  `id` int(10) UNSIGNED NOT NULL,
  `shopkeeper_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_ktp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `shopkeepers`
--

INSERT INTO `shopkeepers` (`id`, `shopkeeper_name`, `owner_name`, `owner_ktp`, `address`, `city`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Liam Bergnaum MD', 'Polly Hoppe', '26610184', '76716 Mosciski Walk\nRoweshire, HI 24641', 'Hagenesport', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(2, 'Prof. Preston Brown V', 'Elliot Treutel', '22039835', '6513 Dach Groves\nEast Sydneetown, KY 87250-5669', 'Raumouth', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(3, 'Branson Little', 'Ms. Cecilia Hessel', '38615108', '1516 Neil Route\nGutmannmouth, WV 14275-6179', 'Lexuston', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(4, 'Gabriel Lowe', 'Dr. Candice Dicki V', '63474077', '73066 Glenda Oval Suite 454\nRolfsonshire, WI 42810', 'Lake Janiya', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(5, 'Lavada Kerluke', 'Mrs. Alexandrea Cummings V', '47312821', '633 Ullrich Expressway\nNorth Rubyeside, NE 78126-7701', 'Connellyland', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(6, 'Percival Jaskolski V', 'Sherwood Jaskolski V', '39589392', '1783 Beaulah Road Suite 569\nNorth Houston, AZ 66170', 'Abdielchester', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(7, 'Ciara Kilback PhD', 'Mr. Casey Kris DDS', '06263942', '4965 Judah Brook\nSouth Filomena, NY 87343-9060', 'Mikeville', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(8, 'Marcos Green', 'Lonie Bode', '88760629', '40345 Estella Port Suite 129\nCamrynhaven, UT 80307', 'North Rachel', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(9, 'Mrs. Amara Fritsch', 'Lexus Zieme Jr.', '10200995', '71135 Price Drives Apt. 134\nWest Ressieview, AR 68564-6973', 'Port Jaden', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(10, 'Margret Buckridge Jr.', 'Mrs. Alexandria Boehm I', '72306505', '39010 Marc Prairie Suite 886\nEast Jodiechester, AZ 32552-9220', 'Port Addietown', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(11, 'Clotilde Gusikowski PhD', 'Mrs. Gudrun Brakus', '21133565', '921 Kilback Trail\nMadalinehaven, AK 67167', 'Port Evansbury', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(12, 'Emmitt Berge', 'Tyrell Ondricka', '87137460', '48728 Aufderhar Junction\nChayaton, OR 78479-9014', 'North Garrick', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(13, 'Telly Johns', 'Brady Windler DDS', '82004354', '80957 Jacobson Ramp Apt. 591\nAshtynside, CT 45634', 'Romagueraview', 'Aktif', '2021-01-19 20:06:10', '2021-01-19 20:06:10'),
(14, 'Carolanne Aufderhar DVM', 'Nicola Muller', '56133301', '6889 O\'Hara Course Apt. 067\nEast Willy, MS 51263', 'South Donna', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(15, 'Jaden Ortiz', 'Phyllis Lakin', '74755059', '53828 Modesta Loop\nSouth Solon, TX 08488', 'Sylvesterstad', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(16, 'Carolina Borer', 'Gretchen Hartmann II', '08331472', '762 Lou Fords Suite 875\nJeanieville, NJ 45619', 'Nienowmouth', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(17, 'Mr. Dejon Rolfson Jr.', 'Dr. Reece Hettinger II', '50555673', '453 Sanford Light\nWest Hershelchester, OK 63112', 'Weissnatfurt', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(18, 'Troy Kuhlman', 'Korey Muller I', '20080655', '3510 Noble Mountains\nJademouth, WA 81537-5110', 'Lake Elroy', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(19, 'Prof. Gail Larkin', 'Pasquale Wilkinson', '71043234', '59852 Brekke Square Apt. 710\nCollinsport, NH 04561-3819', 'Port Mathew', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(20, 'Mrs. Rhoda Hegmann DVM', 'Jamey Stamm', '82657703', '5239 Kuhn Ranch Suite 884\nEast Ruthmouth, CT 90375', 'South Nigelshire', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(21, 'Prof. Claude Bayer V', 'Miss Viva Ferry III', '38229336', '17608 Koepp Brooks\nDoyleview, VA 66562-1809', 'Shirleyhaven', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(22, 'Delpha Wehner', 'Ofelia Huels', '01894967', '76786 Assunta Mill Apt. 124\nHeidenreichton, VA 57705-7254', 'South Lilla', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(23, 'Dr. Kip Gorczany', 'Forest Marquardt', '24855389', '208 Oceane Plains\nWest Mafalda, HI 38848-8071', 'West Marilieport', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(24, 'Mathew Jakubowski', 'Ms. Winnifred Pouros PhD', '62491020', '442 Littel Loaf Suite 502\nNew Wilfredville, TN 67941-7434', 'Dixiemouth', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(25, 'Eryn Crooks', 'Danielle Tillman MD', '05282982', '23927 Langworth Landing Suite 273\nKennediview, OH 07108', 'Homenickfurt', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(26, 'Noemi Littel', 'Kian Hickle', '44375133', '5867 Kody Extensions Suite 601\nCoralieview, CA 60153-4840', 'East Barton', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(27, 'Golden Lind', 'Sofia Kuhic DVM', '11507956', '684 Mayert Grove Suite 915\nWest Lloydmouth, NC 58225', 'Nicolasville', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(28, 'Lucius Ratke', 'Mr. Allen Kohler IV', '84144546', '716 Beier Manors\nNorth Arianeview, NM 89288-1319', 'Lake Arnoldochester', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(29, 'Jayce Mills', 'Dr. Koby Shanahan', '95444659', '52218 Kris Freeway Suite 102\nVonRuedenmouth, ME 03780-0037', 'Tillmanborough', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(30, 'Mrs. Kianna Hirthe III', 'Prof. Javier Romaguera DVM', '94623925', '97875 Elissa Corner\nSouth Nicklaus, NY 64104', 'Lake Moises', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(31, 'Anya Orn', 'Savannah Jenkins DVM', '85264304', '710 Estevan Route\nNew Pollyport, NE 88204-3255', 'West Rodrigoview', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(32, 'Miss Esmeralda Homenick', 'Dr. Isidro Murray', '73301264', '29575 Devyn Trail Apt. 478\nAdrainside, MI 89327-2526', 'West Cruz', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(33, 'Mr. Hipolito Bahringer', 'Prof. Ethel Schamberger', '63881790', '197 Finn Villages Apt. 465\nNorth Dax, TX 77251', 'Emmaleeberg', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(34, 'Cleve Koch', 'Edyth Steuber', '21968464', '40556 Hickle Mountain Suite 532\nLerachester, MD 41128', 'Sporerfort', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(35, 'Prof. Nelson Effertz', 'Prof. Brando Vandervort', '42407591', '965 Jacklyn Lake Suite 105\nPort Daron, WV 39432', 'Pricechester', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(36, 'Sonya VonRueden', 'Agnes Abshire', '65997970', '4715 Kelly Station\nHarveytown, NV 93970', 'Lake Marcelle', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(37, 'Sedrick Jones', 'Miss Annetta Ruecker DDS', '99110161', '896 Muller Summit Suite 859\nNorth Jamey, OK 21470-3863', 'Calistafurt', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(38, 'Ivory Hill', 'Russell Towne', '11248361', '750 Schneider Extensions Apt. 148\nCiaraborough, DE 78443-1334', 'West Melissafort', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(39, 'Mittie Cartwright', 'Dr. Gerson Cummerata MD', '93670890', '822 Alta Shore\nPort Reinhold, MO 13610-7685', 'Jaidatown', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(40, 'Ruthe Runolfsdottir', 'Frank Ortiz', '96353493', '92853 Josefa Club Apt. 993\nElvisfurt, OK 91278', 'West Rosieview', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(41, 'Prof. Alan Lindgren Jr.', 'Kimberly Prohaska', '15821270', '532 Gennaro Rapids\nEast Maxwellville, NM 67219', 'North Ollieville', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(42, 'Nathaniel Muller', 'Germaine Robel', '63011029', '26482 Adriana Bridge\nEast Evefort, VA 33368', 'Lilyhaven', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(43, 'Mr. Brock Christiansen DDS', 'Dr. Godfrey Hauck III', '77130891', '329 Turner Route Suite 283\nCorkeryland, HI 77410-6616', 'Stephonshire', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(44, 'Dayne McLaughlin', 'Ariel Nader', '00399432', '462 Lubowitz Radial\nWilliemouth, NC 29080', 'North Elenor', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(45, 'Lesley Reynolds', 'Dr. Bartholome Hansen DVM', '75484361', '36169 Smith Springs\nDibbertview, NM 97115-6956', 'Karlimouth', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(46, 'Gabrielle Batz', 'Dr. Armani Johnson', '00162784', '51122 Braun Throughway Apt. 431\nMurazikstad, NC 33448-0246', 'Shieldsside', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(47, 'Dr. Destin Abernathy', 'Bria Rolfson', '35611332', '81138 Gisselle Expressway Suite 013\nEnoston, MA 10564-2856', 'Charleymouth', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(48, 'Gaston Abshire', 'Talia Gleason', '46473066', '51603 Neha Ville\nSouth Selina, RI 41984-5242', 'South Treyhaven', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(49, 'Madeline Schneider I', 'Lavada Hand', '92121393', '62313 Jones Forges Suite 592\nTerryview, OH 50397', 'Lake Chauncey', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11'),
(50, 'Mr. Nigel Schamberger', 'Annalise Hyatt', '77329394', '15093 Bartell Wall Suite 485\nJadeton, WV 33352-5777', 'West Erika', 'Aktif', '2021-01-19 20:06:11', '2021-01-19 20:06:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_level` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `account_level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Customer 1', 'customer1@gmail.com', '$2y$10$q9cq.toMqCo9r.mgbysvMe9y9IA7yg.ZE29hYNLNieRLgyo5Gr.I2', 1, NULL, NULL, NULL),
(2, 'Staff 1', 'staff1@gmail.com', '$2y$10$6w5RvzVzrhiudRSmDkp3RucdnJilHt91dvLw0z1tE8o4DOf55C6fW', 2, 'IsuaIY6qPs6LwlxytkeGAnzQN9yvsBkwOOyY6GlN4NAJyDZYBl59Yx0FG0FN', NULL, NULL),
(3, 'Manager', 'manager@gmail.com', '$2y$10$bRX44vJxq9eZXdnQtUPjAeQOuxk/M6TnRWZD9n9L6JcuAGdfvOv8y', 3, NULL, NULL, NULL),
(4, 'Admin', 'admin@gmail.com', '$2y$10$jKjPR/HwkdTLADGN6AfnGeWDlscq6YG2CQloPjtzQ45v4l/QK0I.q', 4, 'OL3mdVuBuQPJ9qwwtJfUek88P7tR00qDZjFS6xreY7nEv4QMRpniGM0dDKLk', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `account_levels`
--
ALTER TABLE `account_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `authentication_log`
--
ALTER TABLE `authentication_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `authentication_log_authenticatable_type_authenticatable_id_index` (`authenticatable_type`,`authenticatable_id`);

--
-- Indeks untuk tabel `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `loyalty_programs`
--
ALTER TABLE `loyalty_programs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indeks untuk tabel `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indeks untuk tabel `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indeks untuk tabel `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indeks untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indeks untuk tabel `shopkeepers`
--
ALTER TABLE `shopkeepers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `account_levels`
--
ALTER TABLE `account_levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `authentication_log`
--
ALTER TABLE `authentication_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `bills`
--
ALTER TABLE `bills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `loyalty_programs`
--
ALTER TABLE `loyalty_programs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `shopkeepers`
--
ALTER TABLE `shopkeepers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

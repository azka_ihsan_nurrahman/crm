<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Admin Route
Route::resource('shopkeeper', 'ShopkeeperController')->middleware('auth');
Route::get('shopkeeper/{shopkeeper}/delete', 'ShopkeeperController@destroy')->name('shopkeeper.delete');
Route::resource('loyaltyprogram', 'LoyaltyProgramController')->middleware('auth');
Route::get('loyaltyprogram/{loyaltyprogram}/delete', 'LoyaltyProgramController@destroy')->name('loyaltyprogram.delete');
Route::resource('item', 'ItemController')->middleware('auth');
Route::get('item/{item}/delete', 'ItemController@destroy')->name('item.delete');
Route::resource('shopkeeperstatus', 'ShopkeeperStatusController')->middleware('auth');
Route::resource('customer', 'CustomerController')->middleware('auth');
Route::resource('useraccess', 'UserAccessController')->middleware('auth');
Route::resource('accesslog', 'AccessLogController')->middleware('auth');

//Staff Route
Route::resource('order', 'OrderController')->middleware('auth');
Route::get('order/{order}/delete', 'OrderController@destroy')->name('order.delete');
Route::resource('bill', 'BillController')->middleware('auth');
Route::post('printreceipt', 'BillController@checkout')->middleware('auth')->name('bill.checkout');

//Management Route
Route::get('financereport', 'FinanceReportController@index')->middleware('auth')->name('financereport.index');
Route::get('financereport/printreport', 'FinanceReportController@printreport')->middleware('auth')->name('financereport.printreport');
Route::get('financereport/cash', 'FinanceReportDetailController@cashindex')->middleware('auth')->name('financereport.cashindex');
Route::get('financereport/debit', 'FinanceReportDetailController@debitindex')->middleware('auth')->name('financereport.debitindex');
Route::get('financereport/credit', 'FinanceReportDetailController@creditindex')->middleware('auth')->name('financereport.creditindex');
Route::get('financereport/cash/printreport', 'FinanceReportDetailController@printcashreport')->middleware('auth')->name('financereport.printcashreport');
Route::get('financereport/debit/printreport', 'FinanceReportDetailController@printdebitreport')->middleware('auth')->name('financereport.printdebitreport');
Route::get('financereport/credit/printreport', 'FinanceReportDetailController@printcreditreport')->middleware('auth')->name('financereport.printcreditreport');
Route::get('omzetreport', 'OmzetReportController@index')->middleware('auth')->name('omzetreport.index');
Route::get('omzetreport/detail/{time}', 'OmzetReportController@show')->middleware('auth')->name('omzetreport.show');
Route::get('omzetreport/printomzetreport', 'OmzetReportController@printomzetreport')->middleware('auth')->name('omzetreport.printomzetreport');
Route::get('omzetreport/detail/{time}/printreportdetail', 'OmzetReportController@printreportdetail')->middleware('auth')->name('omzetreport.printreportdetail');
Route::get('trendreport', 'TrendController@index')->middleware('auth')->name('trendreport.index');
Route::get('trendreport/{group}', 'TrendController@show')->middleware('auth')->name('trendreport.show');
Route::get('trendreport/{group}/printtrendreport', 'TrendController@printtrendreport')->middleware('auth')->name('trendreport.printtrendreport');
Route::get('customerreport', 'CustomerProfileController@index')->middleware('auth')->name('customerreport.index');
Route::get('customerreport/citysegmentreport', 'CustomerProfileController@reportcitysegment')->middleware('auth')->name('customerreport.citysegmentreport');
Route::get('customerreport/professionsegmentreport', 'CustomerProfileController@reportprofessionsegment')->middleware('auth')->name('customerreport.professionsegmentreport');
Route::get('customerreport/showgendersegment/{gender}', 'CustomerProfileDetailController@showgendersegment')->middleware('auth')->name('customerreport.showgendersegment');
Route::get('customerreport/showmaritalsegment/{marital}', 'CustomerProfileDetailController@showmaritalsegment')->middleware('auth')->name('customerreport.showmaritalsegment');
Route::get('customerreport/showagesegment/{age}', 'CustomerProfileDetailController@showagesegment')->middleware('auth')->name('customerreport.showagesegment');
Route::get('customerreport/showgendersegment/{gender}/reportgendersegment', 'CustomerProfileDetailController@reportgendersegment')->middleware('auth')->name('customerreport.reportgendersegment');
Route::get('customerreport/showmaritalsegment/{marital}/reportmaritalsegment', 'CustomerProfileDetailController@reportmaritalsegment')->middleware('auth')->name('customerreport.reportmaritalsegment');
Route::get('customerreport/showagesegment/{age}/reportagesegment', 'CustomerProfileDetailController@reportagesegment')->middleware('auth')->name('customerreport.reportagesegment');

//Upload Picture
Route::get('store/{store}', 'UploadPictureController@upload')->name('upload.picture');
Route::put('store/{store}', 'UploadPictureController@store')->name('store.picture');
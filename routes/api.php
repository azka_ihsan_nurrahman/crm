<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Customer Route

//Auth API
Route::group(['prefix' => 'auth'], function () {
	Route::post('register', 'AuthController@signup');
    Route::post('login', 'AuthController@login');
    Route::post('renew', 'AuthController@renew');
    Route::get('forget', 'AuthController@forget');
    Route::get('reset/{id}', 'AuthController@reset');
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('user', 'AuthController@user');
        Route::get('logout', 'AuthController@logout');
    });
});

//Loyalty Program API
Route::get('loyaltyprogram', 'LoyaltyProgramAPIController@index');
Route::get('loyaltyprogram/{id}', 'LoyaltyProgramAPIController@show');

//Shopkeeper API
Route::get('shopkeeper', 'ShopkeeperAPIController@index');

//Customer API
Route::group(['prefix' => 'customer'], function () {
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('{id}', 'CustomerAPIController@show');
        Route::put('{id}', 'CustomerAPIController@update');      
    });
    Route::delete('{id}', 'CustomerAPIController@destroy');
});

//Item API
Route::group(['prefix' => 'item'], function () {
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('{id}', 'ItemAPIController@show');
    });
});

//Order API
Route::group(['prefix' => 'order'], function () {
    Route::group(['middleware' => 'auth:api'], function() {
    	Route::post('', 'OrderAPIController@store');
        Route::get('{id}', 'OrderAPIController@index');
        Route::put('{id}', 'OrderAPIController@update');
        Route::delete('{id}', 'OrderAPIController@destroy');      
    });
});

//Checkout API
Route::group(['prefix' => 'checkout'], function () {
    Route::group(['middleware' => 'auth:api'], function() {
    	Route::post('', 'CheckoutAPIController@checkout');   
    });
});
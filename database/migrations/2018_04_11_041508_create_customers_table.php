<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('loyalty_program_id')->unsigned();
            $table->string('QR_Code')->nullable();
            $table->string('customer_name');
            $table->date('birth_date');
            $table->integer('age');
            $table->string('gender');
            $table->string('profession');
            $table->string('marital_status');
            $table->string('email');
            $table->text('address');
            $table->string('city');
            $table->string('phone_number');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}

<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Customer 1',
            'email' => 'customer1@gmail.com',
            'password' => bcrypt('customer'),
            'account_level' => 1,
        ]);
        DB::table('users')->insert([
            'name' => 'Staff 1',
            'email' => 'staff1@gmail.com',
            'password' => bcrypt('staff'),
            'account_level' => 2,
        ]);
        DB::table('users')->insert([
            'name' => 'Manager',
            'email' => 'manager@gmail.com',
            'password' => bcrypt('manager'),
            'account_level' => 3,
        ]);
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
            'account_level' => 4,
        ]); 
    }
}

<?php

use Illuminate\Database\Seeder;

class AccountLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('account_levels')->insert([
            'level_description' => 'Customer',
        ]);
        DB::table('account_levels')->insert([
            'level_description' => 'Staff',
        ]);
        DB::table('account_levels')->insert([
            'level_description' => 'Manager',
        ]);
        DB::table('account_levels')->insert([
            'level_description' => 'Admin',
        ]);
    }
}

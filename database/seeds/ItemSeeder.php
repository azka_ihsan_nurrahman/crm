<?php

use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
            'item_name' => 'Kaos Oblong',
            'price' => '50000',
            'description' => 'Kaos Dagadu Oblong Khas Jogja',
            'picture_url' => 'img/250x250.png',
            'stock' => 50,
        ]);
        DB::table('items')->insert([
            'item_name' => 'Baju Berkerah',
            'price' => '100000',
            'description' => 'Baju Kerah Asli Madiun',
            'picture_url' => 'img/250x250.png',
            'stock' => 50,
        ]);
        DB::table('items')->insert([
            'item_name' => 'Celana L',
            'price' => '57000',
            'description' => 'Celana Khas Bali',
            'picture_url' => 'img/250x250.png',
            'stock' => 10,
        ]);
    }
}

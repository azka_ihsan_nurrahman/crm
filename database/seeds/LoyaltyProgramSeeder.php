<?php

use Illuminate\Database\Seeder;

class LoyaltyProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('loyalty_programs')->insert([
            'program_name' => 'Weekend Sale',
            'start_date' => '2018-04-11',
            'end_date' => '2018-04-15',
            'discount' => 50.00,
            'description' => 'Diskon Akhir Minggu',
        ]);
        DB::table('loyalty_programs')->insert([
            'program_name' => 'Weekday Sale',
            'start_date' => '2018-04-16',
            'end_date' => '2018-04-20',
            'discount' => 10.00,
            'description' => 'Diskon Akhir Minggu',
        ]);
        DB::table('loyalty_programs')->insert([
            'program_name' => 'Member Discount',
            'start_date' => '2018-04-01',
            'end_date' => '2018-04-30',
            'discount' => 15.00,
            'description' => 'Diskon Akhir Minggu',
        ]);
    }
}

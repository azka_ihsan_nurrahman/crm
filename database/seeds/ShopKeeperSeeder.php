<?php

use Illuminate\Database\Seeder;

class ShopKeeperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Shopkeeper::class, 50)->create();
    }
}
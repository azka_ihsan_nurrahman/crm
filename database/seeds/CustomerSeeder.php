<?php

use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'customer_name' => 'Customer 1',
            'user_id' => 1,
            'loyalty_program_id' => 1,
            'birth_date' => '1993-04-11',
            'age' => 25,
            'gender' => 'Pria',
            'profession' => 'Programmer',
            'marital_status' => 'Belum Menikah',
            'email' => 'customer1@gmail.com',
            'address' => 'Jl. Pungkur No. 5',
            'city' => 'Surakarta',
            'phone_number' => '082116140638',
            'QR_code' => 'img/250x250.png',
        ]);
    }
}

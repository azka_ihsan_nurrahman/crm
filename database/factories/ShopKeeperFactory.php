<?php

use Faker\Generator as Faker;

$factory->define(App\Shopkeeper::class, function (Faker $faker) {
    return [
        'shopkeeper_name' => $faker->name,
        'owner_name' => $faker->name,
        'owner_ktp' => $faker->ean8,
        'address' => $faker->address,
        'city' => $faker->city,
        'status' => 'Aktif'
    ];
});
